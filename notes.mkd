Ideas for double EP
===================

* two 3-track EPs
* Alternate lush drones with sparse arrangements of noise
  * EP1: drone, sparse, drone
  * EP2: sparse, drone, sparse
* EP1 loosely based around 6th task of Herakles
* Tracks:
  - Stymphalion I
  - Stymphalion II
  - Miasma
* Stymphalion I
  - 22edo drone study
  - habitat of Stymphalian birds
  - metaphor: birds / military helicopter(s)
  - sets stage for 6th task of Herakles
* Stymphalion II
  - series of events
  1. stat model of frogs at Lake Stymphalia
    - frog jumps in grass, likely landing on another frog, who jumps
    - series of Bernoulli trials with initially high, decreasing p
    - possibility of two frogs jumping, setting up a tree
    - each jump generates a froglike "peep"
  2. footsteps, Herakles' arrival
  3. krotala, Hephaestean tool used to flush birds
  4. bird flush
  5. Heraklean arrows
    - bowstring snap
    - whoosh of arrow
    - plunge into birds
    - death!
