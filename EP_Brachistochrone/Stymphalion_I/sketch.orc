sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

	; sine table
gifn1	ftgen	0, 0, 8192, 9, 1, 1, 0
gifn2	ftgen	0, 0, 1024, 9, 1, 1, 90
gifn3	ftgen	0, 0, 1024, 9, 1, 1, 153
gifn4	ftgen	0, 0, 1024, 9, 1, 1, 216
gifn5	ftgen	0, 0, 2^19, 01, "hvac2.wav", 7, 0, 0
gifn6	ftgen	0, 0, 2^19, 01, "copter2.wav", 0, 0, 0
gifn7	ftgen	0, 0, 2^19, 01, "copter3.wav", 0, 0, 0


	; mod global for bass lfo
gklfo	init	1

	; long-tail reverb send channels
garvb1	init	0
garvb2	init	0

	; filter effect send channels
gaflt_L	init	0
gaflt_R	init	0

	instr 1	; chirp TEST SIGNAL
idur	=	p3
iamp	=	p4
ifrq_a	=	p5
ifrq_b	=	p6
itype	=	p7 ; 1=linear, 2=exponential
irvwet	=	p8
irvdry	=	1-irvwet

		; generate frequency function of time
	if	(itype == 1) then ; linear sweep
kfrq	line	ifrq_a, idur, ifrq_b
	elseif	(itype == 2) then ; exponential sweep
kfrq	expon	ifrq_a, idur, ifrq_b
	else
	prints	"invalid input! p7 needs to be from {1,2}.\n"
	endif

		; normalized chirp signal
aosc	oscili	1, kfrq, gifn1
		; signal with (prob less than unity) gain
asig	=	aosc * iamp

		; generate and output dry signal
aout	=	asig * irvdry
	outs	aout, aout

		; generate and send signal to reverb
arvb	=	asig * irvwet
gaflt_L	=	gaflt_L + arvb
gaflt_R	=	gaflt_R + arvb
	endin

	instr 11	; bass
		; TODO: move LFO to send process
idur	=	p3
iamp	=	p4
iedo	=	22	; 12edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
irel	=	p7

idec	=	0
isus	=	1
kenv	adsr	iatt, idec, isus, irel

aosc	oscili	1, ifrq

;ilfo	=	4
;klfo	oscili	1, ilfo
;klfo	=	1 + klfo*0.33

aout	=	aosc * kenv * gklfo * iamp
	outs	aout, aout
	endin

	instr 12	; harmonic buzz pad
idur	=	p3
iamp	=	p4
iedo	=	22	; 22edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
irel	=	p7
irvwet	=	1 ;p10
irvdry	=	1-irvwet

kwetctl	oscil	0.2, 1/20, gifn1
kwetctl	=	kwetctl + 0.6
kdryctl	=	1 - kwetctl

idec	=	0
isus	=	1
kenv	adsr	iatt, idec, isus, irel
;izero	=	0.001
;kenv	expseg	izero, iatt, (1+izero)*isus, idur-(iatt+idec), (1+izero)*isus, idec, izero
;kenv	=	kenv-izero

iinh	=	int(sr/2/ifrq)	; number of harmonics in pulse train
koffset	jitter	1, .5, 3

abuz1	buzz	1, ifrq, iinh, gifn1
abuz2	oscil	.01, ifrq*2+koffset, gifn1
;abuz2	atone	abuz2, 800
;abuz3	oscil	.05, ifrq-ioffset, gifn1
;abuz3	atone	abuz3, 800

abuz	balance	abuz1+abuz2, abuz1
;a	moogvcf	asig, xfco, xres [,iscale, iskip]
aflt	moogvcf	abuz, 1100*kenv, 0.28
asig	balance	aflt, abuz
aout	=	asig * kenv * iamp
aoutdry	=	aout * kdryctl

	outs	aoutdry, aoutdry

garvb1	=	garvb1+(aout*kwetctl);irvwet)
garvb2	=	garvb2+(aout*kwetctl);irvwet)

gaflt_L	=	gaflt_L+aout
gaflt_R	=	gaflt_R+aout
	endin

	instr 21	; read and scale WAV input 
		; TODO: effects send
idur	=	p3
igain	=	p4 ; gain
ifname	=	p5 ; file name for sound input
istart_t	=	p6 ; skip time [seconds]
iatt	=	p7 ; attack / fade in
irel	=	p8 ; release / fade out
iwet_amt	=	p9 ; portion to send to effect
idry_amt	=	1 - iwet_amt

asrc_L,asrc_R \
	soundin	ifname, istart_t

kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0
;kenv	expseg	0.001, iatt, igain+0.001, idur-(iatt+irel), igain+0.001, irel, 0.001
;kenv	=	kenv-0.001

asig_L	=	kenv*asrc_L;, 1200;*1.5 + (asrc_L * anx) + (asrc_L*acauchy)
asig_R	=	kenv*asrc_R;, 1200;*1.5 + (asrc_R * anx) + (asrc_R*acauchy)

aout_L	=	asig_L * idry_amt
aout_R	=	asig_R * idry_amt
	outs	aout_L, aout_R

garvb1	=	garvb1 + (asig_L * iwet_amt)
garvb2	=	garvb2 + (asig_R * iwet_amt)

	endin

	instr 22	; delay line sampler UNFINISHED
idur	=	p3
igain	=	p4 ; gain
ifname	=	p5 ; file name for sound input
istart_t	=	p6 ; file input skip time [seconds]
iatt	=	p7 ; attack / fade in
irel	=	p8 ; release / fade out
ilop	=	p9 ; lopass cutoff (or zero for none)
ihip	=	p10 ; hipass cutoff (or zero for none)
;iwet_amt	=	p11 ; portion to send to effect
;idry_amt	=	1 - iwet_amt

kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0

imaxdel	=	9

adelay	delayr	imaxdel
atap1	deltap	3
atap2	deltap	5
atap3	deltap	7

asrc_L,asrc_R \
	soundin	ifname, istart_t

	if ihip != 0 then
aflt	buthp	asrc_L, ihip
	else
aflt	=	asrc_L
	endif

	if ilop != 0 then
aflt	butlp	aflt, ilop
	endif

	delayw	aflt

aout	=	kenv*(atap1+atap2+atap3)

	outs	aout,aout
	endin

	instr 23	; lookup table sampler UNFINISHED
idur	=	p3
igain	=	p4 ; gain
ifname	=	p5 ; file name for sound input
iskip_t	=	p6 ; file input skip time [seconds]
iatt	=	p7 ; attack / fade in
irel	=	p8 ; release / fade out
;ilop	=	p9 ; lopass cutoff (or zero for none)
;ihip	=	p10 ; hipass cutoff (or zero for none)

isize	=	2^19

ibas	=	1
imod1	=	2
ibeg1	=	0
iend1	=	132300
imod2	=	2
ibeg2	=	0
iend2	=	220500

arL,arR	loscil	1, 1, gifn5, ibas, \
			imod1, ibeg1, iend1, imod2, ibeg2, iend2

kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0

aout_L	=	arL*kenv	
aout_R	=	arR*kenv	

	outs	aout_L,aout_R
	endin

	instr 24 ; crossfade looper - hvac
idur	=	p3
igain	=	p4 ; gain
iatt	=	p5 ; attack / fade in
irel	=	p6 ; release / fade out
ihip	=	p7 ; hipass cutoff (or zero for none)
ilop	=	p8 ; lopass cutoff (or zero for none)

kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0

kpitch	=	1 ; should be 1 but that causes strange problems
		; octave lower and aliasing up near Nyquist freq
		; seems to be a problem with stereo input
kvar	gauss	0.1
kvar	=	kvar+1

istart	=	0
ifad	=	0.05

ildur_L1	=	2.13
ildur_L2	=	6.31
ildur_L3	=	5.11

ildur_R1	=	4.17
ildur_R2	=	7.71
ildur_R3	=	10.41

inlayers	=	6

alp_L1	flooper	1, kpitch*kvar, 5, ildur_L1, ifad, gifn5
alp_L2	flooper	1, kpitch*kvar, 3, ildur_L2, ifad, gifn5
alp_L3	flooper	1, kpitch*kvar, 3, ildur_L3, ifad, gifn5

alp_R1	flooper	1, kpitch*kvar, 4, ildur_R1, ifad, gifn5
alp_R2	flooper	1, kpitch*kvar, 0, ildur_R2, ifad, gifn5
alp_R3	flooper	1, kpitch*kvar, 0, ildur_R3, ifad, gifn5

	if ihip != 0 then
aflt_L1	buthp	alp_L1, ihip
aflt_L2	buthp	alp_L2, ihip
aflt_L3	buthp	alp_L3, ihip
aflt_R1	buthp	alp_R1, ihip
aflt_R2	buthp	alp_R2, ihip
aflt_R3	buthp	alp_R3, ihip
	else
aflt_L1	=	alp_L1
aflt_L2	=	alp_L2
aflt_L3	=	alp_L3
aflt_R1	=	alp_R1
aflt_R2	=	alp_R2
aflt_R3	=	alp_R3
	endif

	if ilop != 0 then
aflt_L1	butlp	aflt_L1, ilop
aflt_L2	butlp	aflt_L2, ilop
aflt_L3	butlp	aflt_L3, ilop
aflt_R1	butlp	aflt_R1, ilop
aflt_R2	butlp	aflt_R2, ilop
aflt_R3	butlp	aflt_R3, ilop
	endif

aloop_L	=	((aflt_L1*.8+aflt_L2*.7+aflt_L3*.65)/inlayers+(aflt_R1*.22+aflt_R2*.35+aflt_R3*.14)/inlayers)*kenv
aloop_R	=	((aflt_L1*.2+aflt_L2*.3+aflt_L3*.35)/inlayers+(aflt_R1*.78+aflt_R2*.65+aflt_R3*.86)/inlayers)*kenv

	outs	aloop_L, aloop_R
	endin

	instr 25 ; crossfade looper - birds
idur	=	p3
igain	=	p4 ; gain
iatt	=	p5 ; attack / fade in
irel	=	p6 ; release / fade out
ihip	=	p7 ; hipass cutoff (or zero for none)
ilop	=	p8 ; lopass cutoff (or zero for none)

kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0

kpitch	=	1 ; should be 1 but that causes problems w/stereo inputs
		; (octave lower and aliasing up near Nyquist freq)
kvar	gauss	0.3
kvar	=	kvar+1

istart	=	0
ifad	=	0.1

ildur_L1	=	2.13
ildur_L2	=	6.31
ildur_L3	=	5.11

ildur_R1	=	4.17
ildur_R2	=	7.71
ildur_R3	=	10.41

istart_t0	=	0
istart_t1	=	3
istart_t2	=	4
istart_t3	=	5

inlayers	=	6

alp_L1	flooper	1, kpitch*kvar, istart_t3, ildur_L1, ifad, gifn6
alp_L2	flooper	1, kpitch*kvar, istart_t1, ildur_L2, ifad, gifn6
alp_L3	flooper	1, kpitch*kvar, istart_t1, ildur_L3, ifad, gifn6

alp_R1	flooper	1, kpitch*kvar, istart_t2, ildur_R1, ifad, gifn6
alp_R2	flooper	1, kpitch*kvar, istart_t0, ildur_R2, ifad, gifn6
alp_R3	flooper	1, kpitch*kvar, istart_t0, ildur_R3, ifad, gifn6

	if ihip != 0 then
aflt_L1	buthp	alp_L1, ihip
aflt_L2	buthp	alp_L2, ihip
aflt_L3	buthp	alp_L3, ihip
aflt_R1	buthp	alp_R1, ihip
aflt_R2	buthp	alp_R2, ihip
aflt_R3	buthp	alp_R3, ihip
	else
aflt_L1	=	alp_L1
aflt_L2	=	alp_L2
aflt_L3	=	alp_L3
aflt_R1	=	alp_R1
aflt_R2	=	alp_R2
aflt_R3	=	alp_R3
	endif

	if ilop != 0 then
aflt_L1	butlp	aflt_L1, ilop
aflt_L2	butlp	aflt_L2, ilop
aflt_L3	butlp	aflt_L3, ilop
aflt_R1	butlp	aflt_R1, ilop
aflt_R2	butlp	aflt_R2, ilop
aflt_R3	butlp	aflt_R3, ilop
	endif

aloop_L	=	((aflt_L1*.8+aflt_L2*.7+aflt_L3*.65)/inlayers+(aflt_R1*.22+aflt_R2*.35+aflt_R3*.14)/inlayers)*kenv
aloop_R	=	((aflt_L1*.2+aflt_L2*.3+aflt_L3*.35)/inlayers+(aflt_R1*.78+aflt_R2*.65+aflt_R3*.86)/inlayers)*kenv

	outs	aloop_L, aloop_R
	endin

	instr 26 ; crossfade looper - helicopter
idur	=	p3
igain	=	p4 ; gain
iatt	=	p5 ; attack / fade in
irel	=	p6 ; release / fade out
ihip	=	p7 ; hipass cutoff (or zero for none)
ilop	=	p8 ; lopass cutoff (or zero for none)

kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0

kpitch	=	1 ; should be 1 but that causes problems w/stereo inputs
		; (octave lower and aliasing up near Nyquist freq)
		; problems appear when attempting to loop from stereo WAV
kvar	gauss	0.3
kvar	=	kvar+1

istart	=	0
ifad	=	0.1

ildur_L1	=	2.13
ildur_L2	=	6.31
ildur_L3	=	5.11

ildur_R1	=	4.17
ildur_R2	=	7.71
ildur_R3	=	10.41

istart_t0	=	0
istart_t1	=	3
istart_t2	=	4
istart_t3	=	5

inlayers	=	6

alp_L1	flooper	1, kpitch*kvar, istart_t3, ildur_L1, ifad, gifn7
alp_L2	flooper	1, kpitch*kvar, istart_t1, ildur_L2, ifad, gifn7
alp_L3	flooper	1, kpitch*kvar, istart_t1, ildur_L3, ifad, gifn7

alp_R1	flooper	1, kpitch*kvar, istart_t2, ildur_R1, ifad, gifn7
alp_R2	flooper	1, kpitch*kvar, istart_t0, ildur_R2, ifad, gifn7
alp_R3	flooper	1, kpitch*kvar, istart_t0, ildur_R3, ifad, gifn7

	if ihip != 0 then
aflt_L1	buthp	alp_L1, ihip
aflt_L2	buthp	alp_L2, ihip
aflt_L3	buthp	alp_L3, ihip
aflt_R1	buthp	alp_R1, ihip
aflt_R2	buthp	alp_R2, ihip
aflt_R3	buthp	alp_R3, ihip
	else
aflt_L1	=	alp_L1
aflt_L2	=	alp_L2
aflt_L3	=	alp_L3
aflt_R1	=	alp_R1
aflt_R2	=	alp_R2
aflt_R3	=	alp_R3
	endif

	if ilop != 0 then
aflt_L1	butlp	aflt_L1, ilop
aflt_L2	butlp	aflt_L2, ilop
aflt_L3	butlp	aflt_L3, ilop
aflt_R1	butlp	aflt_R1, ilop
aflt_R2	butlp	aflt_R2, ilop
aflt_R3	butlp	aflt_R3, ilop
	endif

aloop_L	=	((aflt_L1*.8+aflt_L2*.7+aflt_L3*.65)/inlayers+(aflt_R1*.22+aflt_R2*.35+aflt_R3*.14)/inlayers)*kenv
aloop_R	=	((aflt_L1*.2+aflt_L2*.3+aflt_L3*.35)/inlayers+(aflt_R1*.78+aflt_R2*.65+aflt_R3*.86)/inlayers)*kenv

	outs	aloop_L, aloop_R
	endin

	instr 201	; lfo mod for bass
idur	=	p3
iamp	=	p4
ifrq_beg	=	p5
ifrq_end	=	p6

;kfrq	line	ifrq_beg, idur, ifrq_end
		; hardcoded segments to avoid reinitialization pops
		; TODO: implement individually controlled segments
isegments	=	3
iseglen	=	idur/isegments
kfrq	linseg	0.5, iseglen, 22, iseglen, 2, iseglen, 22
;klfo	oscili	1, kfrq
klfo	lfo	1, kfrq, 1
	; now control the global mod variable
gklfo	=	0.82 + klfo*0.18
	endin

	instr 401 ; bobbing res-filter bank
idur	=	p3
irvwet	=	1
irvdry	=	1-irvwet

	; zig-zag line
;kramp	linseg	1, idur/6, 2.3, idur/6, 1, idur/6, 3.3, idur/6, 1, idur/6, 1.7, idur/6, 0.7

		; sinusoid 1
;	jitter	kamp, kcpsMin, kcpsMax
ijitmin	=	1/3  ; is in Hz
ijitmax	=	1/30 ; is in Hz
kjit_aL	jitter	1/30, ijitmin, ijitmax;
kwav_aL	oscil	0.65, 1/60+kjit_aL, gifn2
kwav_aL	=	-kwav_aL+1.65
kwav_bL	oscil	0.62, 1/(idur*4), gifn1
kwav_bL	=	-kwav_bL+1.22
kwav_1L	=	kwav_aL*kwav_bL

kjit_aR	jitter	1/29, ijitmin, ijitmax;1/5, 1/20
kwav_aR	oscil	0.65, 1/60+kjit_aR, gifn2
kwav_aR	=	-kwav_aR+1.65
kwav_bR	oscil	0.62, 1/(idur*4), gifn1
kwav_bR	=	-kwav_bR+1.22
kwav_1R	=	kwav_aR*kwav_bR

		; sinusoid 2
kjit_bL	jitter	1/60, ijitmin, ijitmax;1/5, 1/20
kwav_cL	oscil	0.65, 1/30+kjit_bL, gifn3
kwav_cL	=	-kwav_cL+1.65
kwav_dL	oscil	0.42, 1/(idur*5), gifn1
kwav_dL	=	-kwav_dL+1.22
kwav_2L	=	kwav_cL*kwav_dL

kjit_bR	jitter	1/61, ijitmin, ijitmax;1/5, 1/20
kwav_cR	oscil	0.65, 1/30+kjit_bR, gifn3
kwav_cR	=	-kwav_cR+1.65
kwav_dR	oscil	0.42, 1/(idur*5), gifn1
kwav_dR	=	-kwav_dR+1.22
kwav_2R	=	kwav_cR*kwav_dR

		; sinusoid 3
kjit_cL	jitter	1/58, ijitmin, ijitmax;1/5, 1/20
kwav_eL	oscil	0.40, 1/30+kjit_cL, gifn4
kwav_eL	=	-kwav_eL+1.40
kwav_fL	oscil	0.32, 1/(idur*3), gifn1
kwav_fL	=	-kwav_fL+1.22
kwav_3L	=	kwav_eL*kwav_fL

kjit_cR	jitter	1/59, ijitmin, ijitmax;1/5, 1/20
kwav_eR	oscil	0.40, 1/30+kjit_cR, gifn4
kwav_eR	=	-kwav_eR+1.40
kwav_fR	oscil	0.32, 1/(idur*3), gifn1
kwav_fR	=	-kwav_fR+1.22
kwav_3R	=	kwav_eR*kwav_fR

iform1	=	800
iform2	=	1890
iform3	=	3414

;aflt1	reson	gaflt, (iform1*kwav_1L)+ioff1, 600
aflt_1L	reson	gaflt_L, (iform1*kwav_1L), 600
aflt_1R	reson	gaflt_R, (iform1*kwav_1R), 650
aflt_2L	reson	gaflt_L, (iform2*kwav_2L), 800
aflt_2R	reson	gaflt_R, (iform2*kwav_2R), 810
aflt_3L	reson	gaflt_L, (iform3*kwav_3L), 450
aflt_3R	reson	gaflt_R, (iform3*kwav_3R), 400

asig_1L	balance	aflt_1L, gaflt_L*0.4
asig_1R	balance	aflt_1R, gaflt_R*0.4
asig_2L	balance	aflt_2L, gaflt_L*0.3
asig_2R	balance	aflt_2R, gaflt_R*0.3
asig_3L	balance	aflt_3L, gaflt_L*0.1
asig_3R	balance	aflt_3R, gaflt_R*0.1

asnd_1L	=	asig_1L*irvwet
asnd_1R	=	asig_1R*irvwet
asnd_2L	=	asig_2L*irvwet
asnd_2R	=	asig_2R*irvwet
asnd_3L	=	asig_3L*irvwet
asnd_3R	=	asig_3R*irvwet

aout_L	=	(asig_1L+asig_2L+asig_3L)*irvdry
aout_R	=	(asig_1R+asig_2R+asig_3R)*irvdry

garvb1	=	garvb1+asnd_1L+asnd_2L+asnd_3L
garvb2	=	garvb2+asnd_1R+asnd_2R+asnd_3R

	outs	aout_L, aout_R
		; clear send channel
gaflt_L	=	0
gaflt_R	=	0
	endin

	instr 901 ; global reverb
idur	=	p3
kroomsize	=	p4 ;0.99
khfdamp	=	p5 ;0.4
kenv	linen	1, 0, idur, 5
arv_l, arv_r	freeverb	garvb1, garvb2, kroomsize, khfdamp

aout_l	=	arv_l*kenv
aout_r	=	arv_r*kenv
	outs	aout_l, aout_r

		; clear send channels
garvb1	=	0
garvb2	=	0
	endin

