<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

	; sine table
gifn1	ftgen	0, 0, 8192, 9, 1, 1, 0
;gifn2	ftgen	0, 0, 1024, 11, 1
gifn2	ftgen	0, 0, 1024, 9, 1, 1, 90
gifn3	ftgen	0, 0, 1024, 9, 1, 1, 153
gifn4	ftgen	0, 0, 1024, 9, 1, 1, 216

	; mod global for bass lfo
gklfo	init	1

	; global reverb send channels
garvb1	init	0
garvb2	init	0

	; filter effect send channel
gaflt	init	0

	instr 11	; bass
		; TODO: move LFO to send process
idur	=	p3
iamp	=	p4
iedo	=	22	; 12edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9

kenv	adsr	iatt, idec, isus, irel

aosc	oscili	1, ifrq

;ilfo	=	4
;klfo	oscili	1, ilfo
;klfo	=	1 + klfo*0.33

aout	=	aosc * kenv * gklfo * iamp
	outs	aout, aout
	endin

	instr 12	; harmonic buzz pad
idur	=	p3
iamp	=	p4
iedo	=	22	; 12edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9
irvwet	=	.5 ;p10
irvdry	=	1-irvwet


kenv	adsr	iatt, idec, isus, irel
;izero	=	0.001
;kenv	expseg	izero, iatt, (1+izero)*isus, idur-(iatt+idec), (1+izero)*isus, idec, izero
;kenv	=	kenv-izero

iinh	=	int(sr/2/ifrq)	; number of harmonics in pulse train
koffset	jitter	1, .5, 3

abuz1	buzz	1, ifrq, iinh, gifn1
abuz2	oscil	.01, ifrq*2+koffset, gifn1
;abuz2	atone	abuz2, 800
;abuz3	oscil	.05, ifrq-ioffset, gifn1
;abuz3	atone	abuz3, 800

abuz	balance	abuz1+abuz2, abuz1
;a	moogvcf	asig, xfco, xres [,iscale, iskip]
aflt	moogvcf	abuz, 1100*kenv, 0.28
asig	balance	aflt, abuz
aout	=	asig * kenv * iamp

	outs	aout*irvdry, aout*irvdry

garvb1	=	garvb1+(aout*irvwet)
garvb2	=	garvb2+(aout*irvwet)

gaflt	=	gaflt+aout
	endin

	instr 201	; lfo mod for bass
idur	=	p3
iamp	=	p4
ifrq_beg	=	p5
ifrq_end	=	p6

kfrq	line	ifrq_beg, idur, ifrq_end
;klfo	oscili	1, kfrq
klfo	lfo	1, kfrq, 1
gklfo	=	0.66 + klfo*0.33
	endin

	instr 401 ; bobbing res-filter bank
idur	=	p3
irvwet	=	1
irvdry	=	1-irvwet

	; zig-zag line
;kramp	linseg	1, idur/6, 2.3, idur/6, 1, idur/6, 3.3, idur/6, 1, idur/6, 1.7, idur/6, 0.7

		; sinusoid 1
kwav_a	oscil	0.65, 1/30, gifn2
kwav_a	=	-kwav_a+1.65
kwav_b	oscil	0.22, 1/(idur*2), gifn2
kwav_b	=	-kwav_b+1.22
kwav1	=	kwav_a*kwav_b

		; sinusoid 2
kwav_c	oscil	0.65, 1/30, gifn3
kwav_c	=	-kwav_c+1.65
kwav_d	oscil	0.22, 1/(idur*2), gifn3
kwav_d	=	-kwav_d+1.22
kwav2	=	kwav_c*kwav_d

		; sinusoid 3
kwav_e	oscil	0.65, 1/30, gifn4
kwav_e	=	-kwav_e+1.65
kwav_f	oscil	0.22, 1/(idur*2), gifn4
kwav_f	=	-kwav_f+1.22
kwav3	=	kwav_e*kwav_f

iform1	=	1000
iform2	=	1090
iform3	=	1114
ioff1	=	200
ioff2	=	600
ioff3	=	300

aflt1	reson	gaflt, (iform1*kwav1)+ioff1, 600
aflt2	reson	gaflt, (iform2*kwav1)+ioff1*1.5, 650
aflt3	reson	gaflt, (iform3*kwav2)+ioff2, 800
aflt4	reson	gaflt, (iform1*kwav2)+ioff2*1.2, 810
aflt5	reson	gaflt, (iform2*kwav3)-ioff3, 450
aflt6	reson	gaflt, (iform3*kwav3)-ioff3*1.3, 400

asig1	balance	aflt1, gaflt;*0.2
asig2	balance	aflt2, gaflt;*0.2
asig3	balance	aflt3, gaflt;*0.1
asig4	balance	aflt4, gaflt;*0.1
asig5	balance	aflt5, gaflt;*0.25
asig6	balance	aflt6, gaflt;*0.25

asnd1	=	asig1*irvwet
asnd2	=	asig2*irvwet
asnd3	=	asig3*irvwet
asnd4	=	asig4*irvwet
asnd5	=	asig5*irvwet
asnd6	=	asig6*irvwet

aout_l	=	(asig1+asig3+asig5)*irvdry
aout_r	=	(asig2+asig4+asig6)*irvdry

garvb1	=	garvb1+asnd1+asnd3+asnd5
garvb2	=	garvb1+asnd2+asnd4+asnd6

	outs	aout_l, aout_r
		; clear send channel
gaflt	=	0
	endin

	instr 901 ; REVERB
idur	=	p3
kroomsize	=	p4 ;0.99
khfdamp	=	p5 ;0.4
kenv	linen	1, 0, idur, 5
arv_l, arv_r	freeverb	garvb1, garvb2, kroomsize, khfdamp

aout_l	=	arv_l*kenv
aout_r	=	arv_r*kenv
	outs	aout_l, aout_r

		; clear send channels
garvb1	=	0
garvb2	=	0
	endin

</CsInstruments>
; ==============================================
<CsScore>
; 1	2	3	4	5	6	7	8	9
;==========================================================================================

; reverb
; instr	t	dur	roomsize	hfdamping
;-----------------------------------------------------------
i 901	0	140.1	.99	.4

; filters
; instr	t	dur
;------------------------------
i 401	0	140.1

; bass mod
; instr	t	dur	DC	frq_beg	frq_end
;-----------------------------------------------------------
i 201	0	-44.625	0	4	42
i 201	+	-44.625	0	42	2
i 201	+	44.625	0	2	22

; bass
; instr	t	dur	amp	pch	att	dec	sus	rel
;------------------------------------------------------------------------------------------
i 11	0	77.35	.2	5.00	16	0	1	4
i 11	77.35	62.4	.	5.09	3	.	.	.


;tenor
i 12	0	140	.2	7.00	14	0	1	2

i 12	0	60.1	.2	7.08	12	.	.	12
i 12	54.1	83.9	.2	7.05	12	.	.	2

i 12	0	94.35	.2	7.21	24	.	.	2
i 12	94.35	45.65	.2	7.09	2	.	.	2

i 12	0	107.1	.2	6.13	8	.	.	2
i 12	107.1	32.9	.2	6.14	2	.	.	2

;i 12	0	52	.3	7.00	4	0	1	2
;i 12	0	52	.2	5.14	8	0	1	2

/*
i 12	50	60	.3	7.04	4	0	1	2
i 12	50	.	.2	5.13	4	.	.	.

i 12	22	8	.2	6.10	3.5	.	.	2.5
i 12	30	12	.	.	5.25	.	.	3.75
i 12	42	18	.	.	7.875	.	.	5.625
i 12	60	12	.	.	5.25	.	.	3.75
i 12	72	8	.	.	3.5	.	.	2.5
*/
</CsScore>
</CsoundSynthesizer>

