<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

	; sine table
gifn1	ftgen	0, 0, 8192, 9, 1, 1, 0

	; mod global for bass lfo
gklfo	init	1


	instr 11	; bass
idur	=	p3
iamp	=	p4
iedo	=	22	; 12edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9

kenv	adsr	iatt, idec, isus, irel

aosc	oscili	1, ifrq

;ilfo	=	4
;klfo	oscili	1, ilfo
;klfo	=	1 + klfo*0.33

aout	=	aosc * kenv * gklfo * iamp
	outs	aout, aout
	endin


	instr 12	; harmonic buzz pad
idur	=	p3
iamp	=	p4
iedo	=	22	; 12edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9

kenv	adsr	iatt, idec, isus, irel

				; highest buzz harmonic LTEQ nyquist
iinh	=	int(sr/2/ifrq)	; number of harmonics in pulse train
kjit	jitter	1, 1, 2.3
abuz1	buzz	1, ifrq, iinh, gifn1
abuz2	oscili	.05, ifrq+kjit*1.1, gifn1
;abuz3	oscili	.05, ifrq-kjit/1.3, gifn1
abuz	balance	abuz1+abuz2, abuz1
;a	moogvcf	asig, xfco, xres [,iscale, iskip]
aflt	moogvcf	abuz, 900*kenv, 0.28
asig	balance	aflt, abuz
aout	=	asig * kenv * iamp

	outs	aout, aout
	endin


	instr 201	; lfo mod for bass
idur	=	p3
iamp	=	p4
ifrq_beg	=	p5
ifrq_end	=	p6

kfrq	line	ifrq_beg, idur, ifrq_end
;klfo	oscili	1, kfrq
klfo	lfo	1, kfrq, 1
gklfo	=	0.66 + klfo*0.33
	endin


	instr 301 ; filter bank 1

	endin


</CsInstruments>
; ==============================================
<CsScore>
;1	2	3	4	5	6	7	8	9
;==========================================================================================

;instr	t	dur	DC	frq_beg	frq_end
;-----------------------------------------------------------
i201	0	42	0	4	34
;i201	+	50	0	34	1

;==========================================================================================
;instr	t	dur	amp	pch	att	dec	sus	rel
;------------------------------------------------------------------------------------------
;bass
i11	0	22	.2	5.00	6	0	1	4
i11	20	20	.	5.09	3	.	.	.

;tenor
i12	0	22	.2	7.00	14	0	1	2
i12	0	.	.2	7.08	21	0	1	2
i12	0	.	.2	7.21	24	0	1	2
i12	0	.	.2	6.13	8	.	.	.

i12	20	22	.2	7.00	4	0	1	2
i12	.	.	.2	7.05	2	0	1	2
i12	.	.	.2	7.09	2	0	1	2
i12	.	.	.2	6.14	8	.	.	.

/*
i12	50	60	.3	7.04	4	0	1	2
i12	50	.	.2	5.13	4	.	.	.

i12	22	8	.2	6.10	3.5	.	.	2.5
i12	30	12	.	.	5.25	.	.	3.75
i12	42	18	.	.	7.875	.	.	5.625
i12	60	12	.	.	5.25	.	.	3.75
i12	72	8	.	.	3.5	.	.	2.5
*/

</CsScore>
</CsoundSynthesizer>

