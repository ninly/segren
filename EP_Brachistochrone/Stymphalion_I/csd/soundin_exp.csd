<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
;====================
<CsInstruments>

sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

garvb1	init	0
garvb2	init	0
garvb3	init	0

	instr 1
idur	=	p3
ifname	=	p4 ; file name for sound input
istart_t	=	p5 ; skip time [seconds]
igain	=	p6 ; gain
iwet_amt	=	p7 ; portion to send to effect
idry_amt	=	1 - iwet_amt
anx	rand	1
;anx	pinkish	awhite
acauchy	cauchy	1

asrc_L,asrc_R\
	soundin	ifname, istart_t

;asig	=	asource * anx
asig_L	buthp	igain*asrc_L, 1200;*1.5 + (asrc_L * anx) + (asrc_L*acauchy)
asig_R	buthp	igain*asrc_R, 1200;*1.5 + (asrc_R * anx) + (asrc_R*acauchy)

aout_L	=	asig_L * idry_amt
aout_R	=	asig_R * idry_amt
	outs	aout_L, aout_R

garvb1	=	garvb1 + (asig_L * iwet_amt)
	endin

	instr 96
irvt	=	p4

arv_pre	tone	garvb1, 1200

arv0	comb	arv_pre, irvt, .23
arv1	comb	arv0, irvt, .1115
arv2	alpass	arv1, irvt, .032
arv3	alpass	arv2, irvt, .029
arv4	alpass	arv3, irvt, .027
arv5	alpass	arv4, irvt, .024
arv6	alpass	arv5, irvt, .017

arl0	tone	arv0, 3000
arl1	tone	arv1, 2000
arl2	tone	arv2, 1500
arl3	tone	arv3, 1000
arl4	tone	arv4, 920
arl5	tone	arv5, 720
arl6	tone	arv6, 420

arv	=	arl0+arl1+arl2+arl3+arl4+arl5+arl6
aout	alpass	arv, irvt, .097
	outs	aout, aout

garvb1	=	0
	endin


</CsInstruments>
;====================
<CsScore>
f1 0 4096 10 1
f2 0 4096 9 1 1 90
f3 0 4096 11 1

;i96	0	67	6.3
;i98	0	20	18
;i99	0	20	4

;instr	t	dur	filename		start_t	gain	wet
;===============================================================================
i1	0	83	"hvac_whine.wav"	7	.5	0

</CsScore>
;====================
</CsoundSynthesizer>

