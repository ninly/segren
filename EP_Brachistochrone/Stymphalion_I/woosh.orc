sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

	instr 1
idur	=	p3
iamp	=	p4
iflt_begl	=	p5
iflt_begh	=	p6
iflt_endl	=	p7
iflt_endh	=	p8

anx	rand	1

klopline	line	iflt_begl, idur, iflt_endl
khipline	line	iflt_begh, idur, iflt_endh

alop	butlp	anx, klopline
ahip	buthp	alop, khipline

asig	balance	ahip, anx

aout	=	asig * iamp

	outs	aout, aout
	endin
