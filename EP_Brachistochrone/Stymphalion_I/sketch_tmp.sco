; 1	2	3	4	5	6	7	8	9
;==========================================================================================

; filters
; instr	t	dur
;------------------------------
i 401	0	46

; reverb
; instr	t	dur	roomsize	hfdamping
;-----------------------------------------------------------
i 901	0	46	.99	.4

; bass mod
; instr	t	dur	DC	frq_beg	frq_end
;-----------------------------------------------------------
;i 201	0	44.625	0	4	42
;i 201	+	44.625	0	42	2
;i 201	+	44.625	0	2	22

; bass
; instr	t	dur	amp	pch	att	dec	sus	rel
;------------------------------------------------------------------------------------------
;i 11	0	77.35	.2	5.00	16	0	1	4
;i 11	77.35	62.4	.	5.09	3	.	.	.


;quasi-equal pentatonic
;i 12	0	3	.4	7.00	1	0	1	1
;i 12	2	.	.4	7.04	.	.	.	.
;i 12	4	.	.4	7.08	.	.	.	.
;i 12	6	.	.4	7.13	.	.	.	.
;i 12	8	.	.4	7.17	.	.	.	.
;i 12	10	.	.4	7.21	.	.	.	.
;i 12	12	.	.4	7.09	.	.	.	.
;i 12	10	.	.4	8.00	.	.	.	.

; pentachords
;i 12	0	5	.2	7.00	1	0	1	1
;i 12	0	.	.	7.04	.	.	.	.
;i 12	0	.	.	7.08	.	.	.	.
;i 12	0	.	.	7.13	.	.	.	.
;i 12	0	.	.	7.17	.	.	.	.

;i 12	4	5	.2	7.02	1	0	1	1
;i 12	4	.	.	7.06	.	.	.	.
;i 12	4	.	.	7.10	.	.	.	.
;i 12	4	.	.	7.15	.	.	.	.
;i 12	4	.	.	7.19	.	.	.	.

;i 12	8	5	.2	7.04	1	0	1	1
;i 12	8	.	.	7.08	.	.	.	.
;i 12	8	.	.	7.12	.	.	.	.
;i 12	8	.	.	7.17	.	.	.	.
;i 12	8	.	.	7.21	.	.	.	.

; diatonic/heptatonic
;i 12	0	3	.4	7.00	1	0	1	1
;i 12	2	.	.4	7.04	.	.	.	.
;i 12	4	.	.4	7.08	.	.	.	.
;i 12	6	.	.4	7.09	.	.	.	.
;i 12	8	.	.4	7.13	.	.	.	.
;i 12	10	.	.4	7.17	.	.	.	.
;i 12	12	.	.4	7.21	.	.	.	.
;i 12	14	.	.4	8.00	.	.	.	.

; Orwell (5\22)

; orwell[9] 3 2 3 2 3 2 3 2 2
;i 12	0	3	.4	7.00	1	0	1	1
;i 12	2	.	.4	7.03	.	.	.	.
;i 12	4	.	.4	7.05	.	.	.	.
;i 12	6	.	.4	7.08	.	.	.	.
;i 12	8	.	.4	7.10	.	.	.	.
;i 12	10	.	.4	7.13	.	.	.	.
;i 12	12	.	.4	7.15	.	.	.	.
;i 12	14	.	.4	7.18	.	.	.	.
;i 12	16	.	.4	7.20	.	.	.	.
;i 12	18	.	.4	8.00	.	.	.	.

; orwell generator chain
;i 12	0	46	.3	7.00	1	0	1	1 ;  1
;i 12	2	3	.	7.05	.	.	.	. ;sm3
;i 12	4	.	.	7.10	.	.	.	. ;pM4
;i 12	6	.	.	7.15	.	.	.	. ;pm6
;i 12	8	.	.	7.20	.	.	.	. ;pA7
;i 12	10	.	.	7.03	.	.	.	. ;pm2
;i 12	12	.	.	7.08	.	.	.	. ;sM3
;i 12	14	.	.	7.13	.	.	.	. ; N5
;i 12	16	.	.	7.18	.	.	.	. ; m7
;i 12	18	.	.	7.01	.	.	.	. ;sm2
;i 12	20	.	.	7.06	.	.	.	. ;pm3
;i 12	22	.	.	7.11	.	.	.	. ; A4
;i 12	24	.	.	7.16	.	.	.	. ;pM6
;i 12	26	.	.	7.21	.	.	.	. ;sM7
;i 12	28	.	.	7.04	.	.	.	. ; M2
;i 12	30	.	.	7.09	.	.	.	. ; N4
;i 12	32	.	.	7.14	.	.	.	. ;sm6
;i 12	34	.	.	7.19	.	.	.	. ;pM7
;i 12	36	.	.	7.02	.	.	.	. ;pd2
;i 12	38	.	.	7.07	.	.	.	. ;pM3
;i 12	40	.	.	7.12	.	.	.	. ;pm5
;i 12	42	.	.	7.17	.	.	.	. ;sM6

; orwell[13] 1 2 2 1 2 2 1 2 2 1 2 2 2
;i 12	0	3	.4	7.00	1	0	1	1
;i 12	1.5	.	.4	7.01	.	.	.	.
;i 12	3	.	.4	7.03	.	.	.	.
;i 12	4.5	.	.4	7.05	.	.	.	.
;i 12	6	.	.4	7.06	.	.	.	.
;i 12	7.5	.	.4	7.08	.	.	.	.
;i 12	9	.	.4	7.10	.	.	.	.
;i 12	10.5	.	.4	7.11	.	.	.	.
;i 12	12	.	.4	7.13	.	.	.	.
;i 12	13.5	.	.4	7.15	.	.	.	.
;i 12	15	.	.4	7.16	.	.	.	.
;i 12	16.5	.	.4	7.18	.	.	.	.
;i 12	18	.	.4	7.20	.	.	.	.
;i 12	19.5	.	.4	8.00	.	.	.	.



/*
i 12	50	60	.3	7.04	4	0	1	2
i 12	50	.	.2	5.13	4	.	.	.

i 12	22	8	.2	6.10	3.5	.	.	2.5
i 12	30	12	.	.	5.25	.	.	3.75
i 12	42	18	.	.	7.875	.	.	5.625
i 12	60	12	.	.	5.25	.	.	3.75
i 12	72	8	.	.	3.5	.	.	2.5
*/
