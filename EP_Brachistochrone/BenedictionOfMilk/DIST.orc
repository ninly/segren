sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

gifn1	ftgen	0, 0, 1024, 9, 1, 1, 0
gifn2	ftgen	0, 0, 257, 9, .5, 1, 270
gifn3	ftgen	0, 0, 257, 9, .5, 1, 270, 1.5, .33, 90, 2.5, .2, 270, 3.5, .143, 90, 4.5, .111, 270

	instr 1
idur	=	p3
iamp	=	p4

iatt	=	1
igain	=	1
irel	=	1
kenv	linseg	0, iatt, igain, idur-(iatt+irel), igain, irel, 0

kdist	oscili	0.5, 1/4, gifn1
asig	oscili	0.3, 440, gifn1
adist	distort	asig, kdist, gifn2
aout	=	adist * kenv * iamp
	outs	aout, aout

	endin

