<CsoundSynthesizer>
<CsOptions>
-o test.wav -W
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2
0dbfs	=	1

giEdo	=	22
giSize	=	2^14
gifn1	ftgen	0, 0, giSize, 9, 1, 1, 0  ; sine
giSaw	ftgen	0, 0, giSize, 10, 1, 1/2, 1/3, 1/4, 1/5, 1/6, 1/7, 1/8, 1/9 ; saw
	; distort transfer function
;		t	?	size	GEN	xint	xamp	h0	h1	h2	h3
giDist	ftgen	0,	0,	513,	13,	.618,	1,	0,	-3,	0,	4

gkcar	init	1
gkmod	init	1
gkndx	init	1
gkenv	init	0
gkres1	init	0
gkres2	init	0

garvb1	init	0
garvb2	init	0


	instr 1	; FM INSTRUMENT
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, giEdo	; quantized frequency input

iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9
kenv	adsr	iatt, idec, isus, irel

aosc1	foscili	0.3, ifrq, gkcar, gkmod, gkndx, gifn1
aosc2	foscili	0.7, ifrq, gkcar, gkmod+4, gkndx, gifn1

aflt1a	reson	aosc1+aosc2, gkres1, 175
aflt1b	butbp	aosc1+aosc2, gkres1, 85
aflt1	balance	(aflt1a+aflt1b)/2, (aosc1+aosc2)/2

aflt2a	butbp	aosc1+aosc2, gkres2, 70
kamt	jitter	70, 1/10, 1
aflt2b	butbp	aosc1+aosc2, gkres2+kamt, 35
aflt2	=	(aflt2a+aflt2b)/2

asum	balance	aflt1+aflt2, aosc1
aflt3	tone	asum, 7000

aout	=	aflt3 * iamp * kenv * gkenv * 1.316
	outs	aout, aout

garvb1	=	garvb1+aout
garvb2	=	garvb2+aout
	endin

	instr 101	; AMPLITUDE CONTROL
idur	=	p3
iamp1	=	p4
iamp2	=	p5
kenv	expseg	iamp1, idur, iamp2
gkenv	=	kenv - 0.01
	endin

	instr 102	; MOD FREQ CONTROL
idur	=	p3
ifrq1	=	p4
ifrq2	=	p5
ispread	=	ifrq2-ifrq1
kamt	jitter	.5, 1/10, 1
kamt	=	kamt + 0.5
kmod	jitter	ispread/2*kamt, 1/5, 1/4
gkmod	=	kmod+((ifrq1+ifrq2)/2)
	printk	10, gkmod
;gkmod	expseg	ifrq1, idur, ifrq2
	endin

	instr 103	; MOD INDEX CONTROL
idur	=	p3
indx1	=	p4
indx2	=	p5
ispread	=	indx2-indx1
kamt	jitter	.5, 1/10, 1
kamt	=	kamt + 0.5
kndx	jitter	ispread/2*kamt, 1/20, 1/5
gkndx	=	kndx+((indx1+indx2)/2)
	printk	10, gkndx
;gkndx	expseg	indx1, idur, indx2
	endin

	instr 104	; RESON1 CUTOFF CONTROL
idur	=	p3
ifrq1	=	p4
ifrq2	=	p5
ispread	=	ifrq2-ifrq1
kamt	jitter	.5, 1/10, 1
kamt	=	kamt + 0.5
kres	jitter	ispread/2*kamt, 1/30, 1/10
gkres1	=	kres+((ifrq1+ifrq2)/2)
	printk	10, gkres1
;gkres1	expseg	ifrq1, idur, ifrq2
	endin

	instr 105	; RESON1 CUTOFF CONTROL
idur	=	p3
ifrq1	=	p4
ifrq2	=	p5
ispread	=	ifrq2-ifrq1
kamt	jitter	.3, 1/10, 1
kamt	=	kamt + 0.7
kres	jitter	ispread/2*kamt, 1/60, 1/30
gkres2	=	kres+((ifrq1+ifrq2)/2)
	printk	10, gkres2
;gkres1	expseg	ifrq1, idur, ifrq2
	endin

	instr 111 ; BASS

idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, giEdo
iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9
irvdry	=	.5;p10
irvwet	=	1-irvdry

kenvAmp	adsr	iatt, idec, isus, irel
kenvDist	expseg	1, iatt+irel, .714, idur-(iatt+irel), .714
kenvDist	=	kenvDist*.7

katt	expseg	.01, iatt*.75, 1, idec, .4, irel-iatt-idec, .01
kjit	jitter	.4, .05, .2
kjit	=	kjit + .6
anxx	pinkish	1
aoscB	oscili	kjit+(anxx*katt*.01), anxx+ifrq, giSaw

asig	=	aoscB*kenvAmp
adist	distort	asig, kenvDist, giDist
abal	balance	adist, asig
aout	=	abal*iamp*irvdry

	outs	aout, aout

garvb1	=	garvb1+(aout*irvwet)
garvb2	=	garvb2+(aout*irvwet)
	endin

	instr 199 ; REVERB
idur	=	p3
kroomsize	=	p4 ;0.99
khfdamp	=	p5 ;0.4
kenv	linen	1, 0, idur, 5
arv_l, arv_r	freeverb	garvb1, garvb2, kroomsize, khfdamp

aout_l	=	arv_l*kenv*1.316
aout_r	=	arv_r*kenv*1.316
	outs	aout_l, aout_r

garvb1	=	0
garvb2	=	0
	endin


</CsInstruments>
; ==============================================
<CsScore>
; reverb
; ins	t	dur	roomsize	hfdamping
i 199	0	472.1	.7	.4

; BASS
; p1	p2	p3	p4	p5	p6	p7	p8	p9
; ========================================================================================
; ins	t	dur	amp	pch	att	dec	sus	rel
i 111	0	30	.6	5.00	2.4	1.3	.8	4
i 111	30	12	.8	4.19	.03	.3	.6	8
i 111	40	100	.6	5.00	1.3	1.3	.9	10
i 111	132	160	.8	5.19	1	1.8	.5	20
i 111	282.5	50	.6	5.06	.03	1.8	.7	20
i 111	305	22	.5	5.08	.3	1.8	.6	8
i 111	310	125	.4	5.06	1.5	1.8	.7	8

; FM instr
; p1	p2	p3	p4	p5	p6	p7	p8	p9
; ========================================================================================
; ins	t	dur	amp	pch	att	dec	sus	rel
; ========================================================================================
i 1	0	235	.3	6.00	.1	.1	1	10
i 1	20	125	.	6.16	10	8	.6	20
i 1	110	150	.	7.03	20	30	.8	14
i 1	130	30	.	7.19	5	4	.8	20
i 1	170	40	.2	7.20	5	4	.8	20
i 1	225	245	.4	6.11	10	4	.9	14
i 1	272	103	.	7.20	25	14	.8	20
i 1	370	80	.	6.09	25	4	.8	10

; amplitude ctrl
; ins	t	dur	start	end
i 101	0	8	.01	1
i 101	452	18	1	.01

; modulation frequency
; ins	t	dur	min	max
i 102	0	470	1.1945	1.2055

; modulation index
; ins	t	dur	min	max
i 103	0	470	3	8

; reson 1 cutoff
; ins	t	dur	min	max
i 104	0	470	200	1100

; reson 2 cutoff
; ins	t	dur	min	max
i 105	0	470	200	1800


</CsScore>
</CsoundSynthesizer>

