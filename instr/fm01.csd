<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

; global function tables
gifn1	ftgen	0, 0, 8193, 9, 1, 1, 0  ; sine

	instr 1	; FM SYNTH

		; input parameters
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 12
icar	=	1
		; effective modulation freq is imod*icar
imod	=	p6
idxmin	=	p7
idxmax	=	p8

		; adsr components
iatt	=	p9
idec	=	p10
isus	=	p11
irel	=	p12
isuslen	=	idur-(iatt+idec+irel)

		; exponential ADSR
kenv	expseg	0.01, iatt, 1.01, idec, isus+0.01, isuslen, isus+0.01, irel, 0.01
kenv	=	kenv - 0.01

		; some slow frequency jitter
anx	rand	.003
anx	=	anx*kenv
kjtrl	jitter	.2, 6, 7
kjtrr	jitter	.2, 6, 7

		; modulation envelope
kndx	linseg	idxmin, iatt*2, idxmax, idur-iatt/2, 2, irel, idxmin

		; fm oscillators
aoscl	foscili	1, ifrq+kjtrl, icar, imod, kndx+kjtrr, gifn1
aoscr	foscili	1, ifrq+kjtrr, icar, imod, kndx+kjtrl, gifn1

asigl	=	aoscl
asigr	=	aoscr

aoutl	=	asigl*kenv*iamp
aoutr	=	asigr*kenv*iamp

	outs	aoutl, aoutr
	endin


</CsInstruments>
; ==============================================
<CsScore>

; ins	t	dur	amp	pch	mod	idxmin	idxmax	att	dec	sus	rel
;================================================================================================================
i 1	0	8	.25	6.00	1.5	0	1	1	0.5	0.8	1
i 1	+	.	.	6.00	1.1	0	1	1	0.5	0.8	1
i 1	.	.	.	6.00	1.1	2	2.5	1	0.5	0.8	1

</CsScore>
</CsoundSynthesizer>
