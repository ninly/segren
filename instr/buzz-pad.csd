<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
0dbfs	=	1

gifn1	ftgen	0, 0, 8192, 9, 1, 1, 0

	instr 1	; A subtractive pad using the buzz opcode
iamp	=	p4
icps	cps2pch	p5, 12
iatt	=	1.1
idec	=	0
isus	=	1    		; ratio of max amp
irel	=	2.0
kenv	adsr	iatt, idec, isus, irel
kenv	=	kenv
;kfltenv	adsr	5, 15, 0.4, 15
iwave	=	1
iinh	=	int(sr/2/icps)	; number of harmonics in pulse train
abuz	buzz	1, icps, iinh, gifn1
aflt	moogvcf	abuz, 1200*kenv, 0.28
asig	balance	aflt, abuz
aout	=	asig * kenv * iamp

	out	aout
        endin

</CsInstruments>
; ==============================================
<CsScore>
;instr	t	dur	amp	pch
i 1	0	4	.2	7.03
i 1	.	.	.	6.00
i 1	.	.	.	8.00
i 1	.	.	.	8.07

i 1	2	.	.	7.07
i 1	.	.	.	5.10
i 1	.	.	.	8.02
i 1	.	.	.	8.05

i 1	4	5	.	7.05
i 1	.	.	.	5.08
i 1	.	.	.	8.00
i 1	.	.	.	8.03


</CsScore>
</CsoundSynthesizer>

