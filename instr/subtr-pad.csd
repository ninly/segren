<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
0dbfs	=	1

gifn1	ftgen	0, 0, 8192, 9, 1, 1, 0

	instr 1
	; A subtractive pad using the vco opcode
iamp	=	p4
icps	cps2pch	p5, 12
iatt	=	1.1
idec	=	0
isus	=	1	; percent of max amp
irel	=	2.0
kenv	adsr	iatt, idec, isus, irel
;kfltenv	adsr	5, 15, 0.4, 15
iwave	=	1
kpw	=	0.5; 0.67, 25, 0.7, p3-25, 0.6
aosc	vco	1, icps, iwave, kpw, gifn1
aflt	moogvcf	aosc, 1200*kenv, 0.28
asig	balance	aflt, aosc
aout	=	asig * kenv * iamp

	out	aout
        endin

</CsInstruments>
; ==============================================
<CsScore>
i1	0	4	.1	7.03
i1	.	.	.	6.00
i1	.	.	.	8.00
i1	.	.	.	8.07

i1	2	.	.	7.07
i1	.	.	.	5.10
i1	.	.	.	8.02
i1	.	.	.	8.05

i1	4	5	.	7.05
i1	.	.	.	5.08
i1	.	.	.	8.00
i1	.	.	.	8.03


</CsScore>
</CsoundSynthesizer>

