<CsoundSynthesizer>
<CsOptions>
-odac
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
0dbfs	=	1

;;  GENERATE GLOBAL FUNCTION TABLES

        ; modified triangle table
gipow1   =       2
gifn1    ftgen   0, 0, 4096, 9, 1, 1, 0, 3, -(.333)^gipow1, 0, 5, (.2)^gipow1, 0, 7, -(0.143)^gipow1, 0, 9, (0.111)^gipow1, 0, 11, -(0.099)^gipow1, 0

        ; sine table
gifn2    ftgen   0, 0, 4096, 9, 1, 1, 0

        instr 1
        ; jitter-based noise-modulated oscillator
icps    cps2pch p4, 12
imax    =       0.2
kamp    adsr    0.25, .4, .8, 1

        ; fast-noisy modulator
kjit1   jitter  1, 400, 510
        ; slow-noisy modulator
kjit2   jitter  1, 6, 6.1
        ; weight and superpose noisy modulators
kjit    =       1 + kjit1*0.03 + kjit2*0.013

aosc    oscil   kamp*imax, kjit*icps, gifn1
asig    =       aosc
        out     asig
        endin

</CsInstruments>
; ==============================================
<CsScore>
i1      0       3       8.09
i1      +       .       8.11
i1      +       .       7.08
i1      +       .       7.04


</CsScore>
</CsoundSynthesizer>

