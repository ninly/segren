<CsoundSynthesizer>
<CsOptions>
-o test.wav -W
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
;0dbfs	=	1

gk_cmd	init	200	; command signal r(t)
gk_err	init	0	; error signal e(t)
gk_out	init	100	; output signal y(t)
gk_ctrl	init	0


	instr 1	; integrated pid-control
idur	=	p3
i_cmd	=	p4
i_Kp	=	p5	; proportional coefficient
i_Ki	=	p6	; integral coefficient
i_Kd	=	p7	; derivative coefficient

k_y	init	440

k_err	=	i_cmd - k_y

k_integ	integ	k_err
k_diff	diff	k_err

k_P	=	k_err * i_Kp
k_I	=	k_integ * i_Ki
k_D	=	k_diff * i_Kd

k_u	=	k_P + k_I + k_D

k_y	=	k_y * k_u

	printks	"P: %3.2f,\tI: %3.2f,\tD: %3.2f,\tG: %3.2f,\t E: %3.2f,\tU: %3.2f,\tR: %f,\n", 1/sr, k_P, k_I, k_D, i_cmd, k_err, k_u, k_y

	endin

	instr 2	; command input
idur	=	p3
icmd	=	p4
gk_cmd	=	icmd
	endin


	instr 101	; PID control
i_Kp	=	p4	; proportional coefficient
i_Ki	=	p5	; integral coefficient
i_Kd	=	p6	; derivative coefficient

			; error signal
gk_err	=	gk_cmd - gk_out

k_integ	integ	gk_err
k_diff	diff	gk_err

			; control signal components
k_P	=	gk_err * i_Kp
k_I	=	k_integ * i_Ki
k_D	=	k_diff * i_Kd
			; control signal
gk_ctrl	=	k_P + k_I + k_D
			; modify output signal
gk_out	=	gk_out * gk_ctrl
;gk_cmd	=	gk_cmd+1

	printks	"P: %3.2f,\tI: %3.2f,\tD: %3.2f,\tG: %3.2f,\t E: %3.2f,\tU: %3.2f,\tR: %f,\n", 1/sr, k_P, k_I, k_D, gk_cmd, gk_err, gk_ctrl, gk_out
;	printks	"INTEG: %f, DIFF: %f\n", .005, k_integ, k_diff
	out	a(gk_out)
	endin

</CsInstruments>
; ==============================================
<CsScore>
;i1	t	dur	cmd	Kp	Ki	Kd
;i 1	0	.01	220	1	0	0

;i 2	0	.1	220
;i 2	+	.	440

; pid	t	dur	Kp	Ki	d
i 101	0	.01	1	1	1


</CsScore>
</CsoundSynthesizer>
