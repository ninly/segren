<CsoundSynthesizer>
<CsOptions>
-o test.wav -W
</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
0dbfs	=	1

gk_cmd	init	0	; command: control input
gk_pch	init	0	; current value

ga_cmd	init	0	; command: control input
ga_pch	init	0	; current value

	instr 1	; PID pitch control
idur	=	abs(p3)
iamp	=	p4
iedo	=	12	; 12edo score input
ifrq	cps2pch	p5, iedo	; quantized frequency input

iatt	=	p6
idec	=	p7
isus	=	p8
irel	=	p9

aenv	adsr	iatt, idec, isus, irel
aenv	line	0, idur, 1

aval	=	aenv
ainteg	integ	aval
aderiv	diff	aval
alinvers	integ	aderiv

	prints	"kval\tkinteg\tkderiv\n"
	printks	"%3.2f,\t%3.2f,\t%3.2f,\t%3.2f\n", .1, aval, ainteg, aderiv, alinvers
	prints	"%f", aderiv
	endin


	instr 101	; PID controller
iK_P	=	p4	; proportional coefficient
iK_I	=	p5	; integral coefficient
iK_D	=	p6	; derivative coefficient
kE	=	gk_cmd - gk_pch	; error signal
k_integ	integ	kE
k_diff	diff	kE
kP	=	kE * iK_P
kI	=	k_integ * iK_I
kD	=	k_diff * iK_D
	endin

	instr 201 ; PLL-based controller
acps, alock	plltrack ga_cmd, 1
ga_cmd	=	ga_cmd+1

	printks "%3.2f\t%3.2f\n", .1, k(acps), k(alock)
	endin

</CsInstruments>
; ==============================================
<CsScore>

;instr	t	dur	amp	frq	att	dec	sus	rel
;====================================================================================================

;i1	0	10	1	440	2	0	1	4
i201	1	1

</CsScore>
</CsoundSynthesizer>

