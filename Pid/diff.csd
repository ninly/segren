<CsoundSynthesizer>
<CsOptions>
; Select audio/midi flags here according to platform
-o test.wav -W
;-iadc    ;;;uncomment -iadc if RT audio input is needed too
; For Non-realtime ouput leave only the line below:
; -o diff.wav -W ;;; for file output any platform
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 32
nchnls = 2
0dbfs = 1

	instr 1
asig1,asig2	diskin2	"example.wav", 1
adif	diff	asig1
aint	integ	asig1
aint2	=	aint*.0003617158			;way too loud
ainv	integ	adif
	out	aint2, asig1
	endin

	instr 2
idur	=	p3
ksig	line	0, idur, 1
kdif	diff	ksig
kint	integ	ksig/32000
kdif2	=	kdif*30000
	out	a(kint), a(ksig)
	endin


</CsInstruments>
<CsScore>

i 2 0 1


e

</CsScore>
</CsoundSynthesizer>
