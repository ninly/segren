gk_cmd	init	; command signal r(t)
gk_out	init	; output signal y(t)

	instr 1	; PID control
k_Kp	=	p4	; proportional coefficient
k_Ki	=	p5	; integral coefficient
k_Kd	=	p6	; derivative coefficient

			; error signal
k_err	=	gk_cmd - gk_out

k_integ	integ	k_err
k_diff	diff	k_err

			; control signal components
k_P	=	k_err * k_Kp
k_I	=	k_err * k_Ki
k_D	=	k_err * k_Kd

			; control signal
k_ctrl	=	k_P + k_I + k_D

			; modify output signal
gk_out	=	gk_out * k_ctrl
	endin
