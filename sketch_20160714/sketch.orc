sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2
0dbfs	=	1

gaton	init	0
garvb	init	0
gapanL	init	.5
gapanR	init	.5
gabel	init	0

gifn1	ftgen	0, 0, 257, 9, .5, 1, 270
gifn2	ftgen	0, 0, 257, 9, .5, 1, 270, 1.5, .33, 90, 2.5, .2, 270, 3.5, .143, 90, 4.5, .111, 270

	instr 1	; drone 1
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 22
iatt	=	p6
irel	=	p7

iwet	=	.3
idry	=	1-iwet

kenv	expseg	0.01, iatt, 1, idur-(iatt+irel), 1, irel, 0.01

krnd1	gauss	1
krnd2	gauss	1
krnd3	betarand	1, .9, .1

iamp1	=	0.2
iseed1	=	1.1
isel1	=	0.1
ioffset1	=	0.2

krwb1	rand	iamp1, iseed1, isel1, ioffset1
;krwb1	=	krwb1+.2

iamp2	=	0.4
iseed2	=	1.2
isel2	=	0.2
ioffset2	=	0.4

krwb2	rand	iamp2, iseed2, isel2, ioffset2;.4, 1.2, .2, 0.4
;krwb2	=	krwb2+.4

kwarb1	oscil	krwb2, krwb1, 2
kwarb	oscil	.3, kwarb1, 3
kvol	=	kwarb+.3
;printk	.5, kvol

;
;ares	foscil	xamp, kcps, xcar, xmod, kndx, ifn [, iphs]
;
kfrq	=	ifrq+krnd3
aosc1a	foscil	1, 1, kfrq, kfrq*0.75, kvol*1.5, 1
aosc2a	oscil	1, ifrq*(1+(krnd1*.01)), 1
aosc3a	oscil	1, ifrq*(1-(krnd2*.01)), 1
aosca	balance	aosc1a+aosc2a+aosc3a, aosc1a

aflt1	reson	aosca, ifrq*5.25, 420
aflt2	reson	aosca, ifrq*3.236, 120
aflt3	reson	aosca, ifrq*2.1, 20
aflt4	balance	aflt1+aflt2+aflt3, aosca
aflt5	tone	aflt4, ifrq*8.4

asig	distort	aflt5, kvol+0.2, gifn1

aout	=	asig*kenv*iamp
aoutL	=	aout*gapanR*idry
aoutR	=	aout*gapanL*idry
	outs	aoutL, aoutR

garvb	=	garvb+(aout*iwet)
gaton	=	(gaton+(1-kvol))*.5
	;printk	.5, k(gaton)
	endin

	instr 2	; drone 2
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 22
iatt	=	p6
irel	=	p7

kenv	expseg	0.01, iatt, 1, idur-(iatt+irel), 1, irel, 0.01
;kenv	expseg	0.01, 4, 1, idur-8, 1, 4, 0.01

agauss	gauss	2
asmoo	tone	agauss, 20
awarb	oscil	0.1, asmoo, 2
aosc	oscil	1, ifrq+(1+awarb), 3

asig	distort	aosc, k(gaton)+0.3, gifn1

aout	balance	asig, iamp*gaton*kenv
aoutL	=	aout*gapanL
aoutR	=	aout*gapanR
	outs	aoutL, aoutR

garvb	=	garvb+(aout*.6)
	endin

	instr 3
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 22

kdec	line	1, idur, 0

ivib1	cps2pch	10.00, 53
ivib2	cps2pch	10.12, 53
ivib_amt	=	ivib2-ivib1
kvib	oscil	ivib_amt, 3, 2

;kcho	jitter	.1, 100, 500
kcho	gauss	1
kcho	=	kcho+1

iparm1	=	1
iparm2	=	1
aplk	pluck	1, ifrq*kcho, ifrq, 3, 1;, iparm1,; iparm2

aflt1	tone	aplk, ifrq
aflt2	reson	aplk, 3000+kvib, 80
aflt3	reson	aplk, 7000+(kvib*.5), 210
aflt4	reson	aplk, 11000+(kvib*.3), 330
aflt5	balance	aflt1+aflt2+aflt3+aflt4, aplk
aflt	tone	aflt5, ifrq*4

kenv	linen	1, idur/2, idur, 12
asig	=	aflt*kenv

aout	=	(asig)*iamp*kdec

	outs	aout*0.7,aout*1.42857

garvb	=	garvb+(aout*.6)
	endin

	instr 5	; bell
ilen	=	p3
iamp	=	p4

ihum	=	p5
iprime	=	p6
itierce	=	p7
iquint	=	p8
isquint	=	p9
ionom	=	p10
inom	=	p11

iwet	=	p12
idry	=	1-p12

idur	=	9.5

;kjitter	gauss	1.6
;kjitter	=	2+kjitter

krange	=	1
kalpha	=	0.6
kbeta	=	0.4
kjitter	betarand	krange, kalpha, kbeta

;kenv	linseg	.6, ilen*.3, 1, ilen*.6, 1, ilen*.1, 0
;kenv	linseg	.1, ilen*.3, 1, ilen*.4, 1, ilen*.3, 0
kenv	linseg	1, ilen-1, 1, 1, 0

	print	cent(ihum)
	print	cent(iprime)
	print	cent(itierce)
	print	cent(iquint)
	print	cent(isquint)
	print	cent(ionom)

start:	if 0.3 <= kjitter*idur then
	timout	0, idur*i(kjitter), continue
	reinit	start
	endif

continue:
anx	gauss	.1
knx	expseg	0.01, .01, .1, .08, .01

anflt1	butbp	anx, inom*1.3, 2000
anoise	=	1+anflt1
anom	=	inom;*anoise

anflt2	reson	anx*knx, inom*18, 2600
astrike	=	0;anflt2*.5

;k1	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k1	expseg	0.0058824, .03, 0.88235, .5, 0.35294, 1.5, 0.58824, idur-2, 0.0058824
;k2	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2	expseg	0.0058824, .03, 0.88235, .5, 0.35294, 1.5, 0.58824, idur-2, 0.0058824
;k3	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k3	expseg	0.0058824, .03, 0.88235, .5, 0.35294, 1.5, 0.58824, idur-2, 0.0058824
;k4	expseg	0.01, .02, 1.6, .5, .6, 1.5, .8, idur-2, 0.01
k4	expseg	0.0058824, .02, 0.94118, .5, 0.35294, 1.5, 0.47059, idur-2, 0.0058824
;k5	expseg	0.01, .02, 1.6, .5, .6, 1.5, .7, idur-2, 0.01
k5	expseg	0.0058824, .02, 0.94118, .5, 0.35294, 1.5, 0.41176, idur-2, 0.0058824
;k6	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k6	expseg	0.0058824, .01, 1.0, .5, 0.35294, 1.5, 0.41176, idur-2, 0.0058824
;k7	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7	expseg	0.0058824, .01, 1.0, .5, 0.35294, 1.5, 0.41176, idur-2, 0.0058824

a1	oscil	0.12*k1, anom*cent(ihum), 2
a2	oscil	0.4*k2, anom*cent(iprime), 2
a2d	oscil	0.2*k2, anom*cent(iprime+4), 2
a3	oscil	0.85*k3, anom*cent(itierce), 2
a4	oscil	0.05*k4, anom*cent(iquint), 2
a5	oscil	1.0*k5, anom, 2
;a5a	oscil	0.1*k5, anom*cent(490), 2
;a5b	oscil	0.2*k5, anom*cent(510), 2
a6	oscil	0.1*k6, anom*cent(isquint), 2
a7	oscil	0.07*k7, anom*cent(ionom), 2

;asig	=	(a1+a2+a2d+a3+a4+a5+a5a+a5b+a6+a7)/10+astrike
asig	=	(a1+a2+a2d+a3+a4+a5+a6+a7)/8+astrike

aout	=	asig*iamp*2.85*kenv

	outs	aout*idry, aout*idry
gabel	=	gabel+(aout*iwet)
	endin

	instr 11
idur	=	p3
iamp	=	p4
i_ed2	=	22
ifrq	cps2pch	p5, i_ed2

iatt	=	p6
irel	=	p7

kdec	line	1, idur, 0

;ivib1	cps2pch	10.00, 53
;ivib2	cps2pch	10.12, 53
;	print	ivib1
;	print	ivib2
;ivib_amt	=	ivib2-ivib1

	; sine wave "vibrato" base added to filter cutoff freqs
ivib_amt	=	178
kvib	oscil	ivib_amt, 1/11.1, 2

;kcho	jitter	.1, 100, 500
kcho	gauss	1
kcho	=	kcho+1

iparm1	=	1
iparm2	=	1
aplk	pluck	1, ifrq*kcho, ifrq, 3, 1;, iparm1,; iparm2

aflt1	tone	aplk, ifrq*3
aflt2	reson	aplk, 3000+kvib, 800
aflt3	reson	aplk, 7000-(kvib*.5), 2100
aflt4	reson	aplk, 11000+(kvib*.3), 3300
aflt5	balance	aflt1+aflt2+aflt3+aflt4, aplk
aflt	tone	aflt5, ifrq*4

kenv	linen	1, iatt, idur, irel
asig	=	aflt*kenv

aout	=	(asig)*iamp*kdec

	outs	aout, aout

garvb	=	garvb+(aout*.6)
	endin


	instr 97		;stereo dispersion and reverb
			;for modeled bell (instr 5)
idur	=	p3
irvt	=	p4

kenv	linseg	1, idur-4, 4, 1, 0

adel_line	delayr	.01
atap	deltap	.007
	delayw	gabel

adlyL	=	gabel
adlyR	=	atap

arvbL1	comb	adlyL, irvt, .043
arvbL2	comb	adlyL, irvt, .049
arvbL3	comb	adlyL, irvt, .031
arvbL4	comb	adlyL, irvt, .027
arvbL5	comb	adlyL, irvt, .019
arvbLm	=	(arvbL1+arvbL2+arvbL3+arvbL4+arvbL5)*.2
arvbLa	alpass	arvbLm, irvt, .059
arvbLb	alpass	arvbLm, irvt, .051
arvbLf	=	(arvbLa+arvbLb)*.5
arvbL	tone	arvbLf, 4000

arvbR1	comb	adlyR, irvt, .047
arvbR2	comb	adlyR, irvt, .051
arvbR3	comb	adlyR, irvt, .041
arvbR4	comb	adlyR, irvt, .037
arvbR5	comb	adlyR, irvt, .017
arvbRm	=	(arvbR1+arvbR2+arvbR3+arvbR4+arvbR5)*.2
arvbRa	alpass	arvbRm, irvt, .057
arvbRb	alpass	arvbRm, irvt, .049
arvbRf	=	(arvbRa+arvbRb)*.5
arvbR	tone	arvbRf, 4000

asigL	=	arvbL*kenv
asigR	=	arvbR*kenv
	;outs	arvbL*1.11, arvbR*.9
	outs	asigL, asigR
gabel	=	0
	endin

	instr 98		;panning control instr for drones
			;(instr 1 & instr 2)
idur	=	p3

kjtr	jitter	0.5, 0.2, 0.5
kjtr	=	(kjtr+1)*.2

kpan	oscil	.1, kjtr, 3
kpan	=	kpan+.5

gapanL	=	sqrt(kpan)
gapanR	=	sqrt(1-kpan)
	endin

	instr 99		;stereo dispersion and reverb 
			;for pluck (instr 3)
idur	=	p3
irvt	=	p4

adel_line	delayr	0.4
;kvar	oscili	0.1, 1.5, 1
;kvar1	=	kvar+1
;kvar2	=	-kvar+1
;=====			LEFT
;amtapV1	deltap	0.2710*kvar1
amtap1	deltap	0.0830
amtap2	deltap	0.0620
amtap3	deltap	0.2858
amtap4	deltap	0.3823
amtap5	deltap	0.0203
amtap6	deltap	0.0439
;=====			RIGHT
;amtapV2	deltap	0.2670*kvar2
amtap7	deltap	0.0846
amtap8	deltap	0.2675
amtap9	deltap	0.2067
amtap10	deltap	0.0187
amtap11	deltap	0.1637
amtap12	deltap	0.2276
	delayw	garvb
adiffL	=	(amtap1+amtap2+amtap3+amtap4+amtap5+amtap6)/6
adiffR	=	(amtap7+amtap8+amtap9+amtap10+amtap11+amtap12)/6

arvbL	reverb	adiffL, irvt
arvbR	reverb	adiffR, irvt

aechoL	comb	garvb*.6+arvbR, 3.4, .6
aechoR	comb	garvb*.6+arvbL, 3.8, .6

alopL	tone	arvbL+aechoL, 8000
alopR	tone	arvbR+aechoR, 8000
;alopL	=	arvbL+aechoL
;alopR	=	arvbR+aechoR

kenv	linseg	1, idur-1, 1, 1, 0 ;envlp to remove the pop caused by instr shutting off

aoutL	=	alopL*kenv
aoutR	=	alopR*kenv

	outs	aoutL*.54, aoutR*.667
garvb	=	0
	endin
