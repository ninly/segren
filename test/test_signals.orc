sr	=	44100
ksmps	=	1
nchnls	=	2
0dbfs	=	1

gifn1	ftgen	0, 0, 8192, 9, 1, 1, 0

	instr 1	; TEST SIGNAL
		; chirp/sweep
idur	=	p3
iamp	=	p4
ifrq_a	=	p5
ifrq_b	=	p6
itype	=	p7 ; 1=linear, 2=exponential
irvwet	=	p8
irvdry	=	1-irvwet

		; generate frequency function of time
	if	(itype == 1) then ; linear sweep
kfrq	line	ifrq_a, idur, ifrq_b
	elseif	(itype == 2) then ; exponential sweep
kfrq	expon	ifrq_a, idur, ifrq_b
	else
	prints	"invalid input! p7 needs to be from {1,2}.\n"
	endif

		; normalized chirp signal
aosc	oscili	1, kfrq, gifn1
		; signal with (prob less than unity) gain
asig	=	aosc * iamp

		; generate and output dry signal
aout	=	asig * irvdry
	outs	aout, aout

		; generate and send signal to reverb
arvb	=	asig * irvwet

; SEND WET SIGNAL TO UNIT UNDER TEST HERE

;garvb1	=	garvb1 + arvb
;garvb2	=	garvb2 + arvb
;gaflt	=	gaflt + arvb
	endin
