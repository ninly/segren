<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
0dbfs	=	1


        instr   1
        ;;;;;;;;;
kamp    =       0.6
icps    cps2pch p4, 22
ifn     =       p5
asig    oscil   kamp, icps, ifn
        outs    asig, asig
        endin

</CsInstruments>
; ==============================================
<CsScore>

f1      0       16384   10      1                                          ; Sine
f2      0       16384   10      1 0.5 0.3 0.25 0.2 0.167 0.14 0.125 .111   ; Sawtooth
f3      0       16384   10      1 0   0.3 0    0.2 0     0.14 0     .111   ; Square
f4      0       16384   10      1 1   1   1    0.7 0.5   0.3  0.1          ; Pulse

;1      2       3       4       5       6       7       8
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
i1      0       1       8.02    1
i1      2       .       8.04    2
i1      4       .       8.06    3
i1      6       .       8.08    4

e
</CsScore>
</CsoundSynthesizer>

