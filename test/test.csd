<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
; ==============================================
<CsInstruments>

sr	=	44100
ksmps	=	1
;nchnls	=	2
0dbfs	=	1


        instr   1
        ;;;;;;;;;
kamp    =       0.6
kcps    =       440
ifn     =       p4
asig    oscil   kamp, kcps, ifn
        outs    asig, asig
        endin

</CsInstruments>
; ==============================================
<CsScore>

f1      0       16384   10      1                                          ; Sine
f2      0       16384   10      1 0.5 0.3 0.25 0.2 0.167 0.14 0.125 .111   ; Sawtooth
f3      0       16384   10      1 0   0.3 0    0.2 0     0.14 0     .111   ; Square
f4      0       16384   10      1 1   1   1    0.7 0.5   0.3  0.1          ; Pulse

i1      0       2       1
i1      3       2       2
i1      6       2       3
i1      9       2       4

e
</CsScore>
</CsoundSynthesizer>

