<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
;====================
<CsInstruments>
	
sr	=	44100
kr	=	4410
ksmps	=	10

gaton	init	0

	instr 1	
idur	=	p3
iamp	=	p4
ifrq	=	(p5)

kvol	oscil	.5, 1, 1
kvol	=	kvol+.5

asig	oscil	kvol, ifrq, 1

aout	=	asig*iamp
	out	aout

gaton	=	(1-kvol)
	endin

	instr 2
idur	=	p3
iamp	=	p4
ifrq	=	(p5)

asig	oscil	1, ifrq, 1

aout	=	asig*iamp*gaton
	out	aout
	endin

</CsInstruments>
;====================
<CsScore>
f1 0 16384 10 1

i1	0	10	10000	440
i2	0	10	10000	330
</CsScore>
;====================
</CsoundSynthesizer>

