<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

gaton	init	0
garvb	init	0
gapanL	init	.5
gapanR	init	.5
gabel	init	0

	instr 1	
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53
iatt	=	p6
idec	=	p7

iwet	=	.3
idry	=	1-iwet

kenv	expseg	0.01, iatt, 1, idur-(iatt+idec), 1, idec, 0.01

krnd1	gauss	1
krnd2	gauss	1
krnd3	betarand	1, .9, .1

krwb1	rand	.2, .1, .1
krwb1	=	krwb1+.2
krwb2	rand	.4, .2, .2
krwb2	=	krwb2+.4

kwarb1	oscil	krwb2, krwb1, 2
kwarb	oscil	.5, kwarb1, 3
kvol	=	kwarb+.5

kfrq	=	ifrq+krnd3
aosc1a	foscil	1, 1, kfrq, kfrq*0.75, kvol*1.5, 1
aosc2a	oscil	1, ifrq*(1+(krnd1*.01)), 1
aosc3a	oscil	1, ifrq*(1-(krnd2*.01)), 1
aosca	balance	aosc1a+aosc2a+aosc3a, aosc1a

aflt1	reson	aosca, ifrq*5.25, 420
aflt2	reson	aosca, ifrq*3.236, 120
aflt3	reson	aosca, ifrq*2.1, 20
aflt4	balance	aflt1+aflt2+aflt3, aosca
aflt5	tone	aflt4, ifrq*8.4

asig	=	aflt5*kvol

aout	=	asig*kenv*iamp
aoutL	=	aout*gapanR*idry
aoutR	=	aout*gapanL*idry
	outs	aoutL, aoutR

garvb	=	garvb+(aout*iwet)
gaton	=	(gaton+(1-kvol))*.5
	endin

	instr 2
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53

kenv	expseg	0.01, 4, 1, idur-8, 1, 4, 0.01

agauss	gauss	2
asmoo	tone	agauss, 20
awarb	oscil	100, asmoo, 2
aosc	oscil	1, ifrq+awarb, 3

asig	=	aosc

aout	balance	asig, iamp*gaton*kenv
aoutL	=	aout*gapanL
aoutR	=	aout*gapanR
	outs	aoutL, aoutR

garvb	=	garvb+(aout*.6)
	endin

	instr 3
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53

kdec	line	1, idur, 0

ivib1	cps2pch	10.00, 53
ivib2	cps2pch	10.12, 53
ivib_amt	=	ivib2-ivib1
kvib	oscil	ivib_amt, 3, 2

aplk	pluck	1, ifrq, ifrq, 0, 1

aflt1	tone	aplk, ifrq
aflt2	reson	aplk, 3000+kvib, 80
aflt3	reson	aplk, 7000+(kvib*.5), 210
aflt4	reson	aplk, 11000+(kvib*.3), 330
aflt5	balance	aflt1+aflt2+aflt3+aflt4, aplk
aflt	tone	aflt5, ifrq*4

asig	=	aflt

aout	=	(asig)*iamp*kdec

	outs	aout*0.7,aout*1.42857

garvb	=	garvb+(aout*.6)
	endin

	instr 5	
ilen	=	p3
iamp	=	p4
ihum	=	p5
iprime	=	p6
itierce	=	p7
iquint	=	p8
isquint	=	p9
ionom	=	p10
inom	=	p11

iwet	=	p12
idry	=	1-p12

idur	=	9.5

kjitter	gauss	.2
kjitter	=	1+kjitter

kenv	linseg	.6, ilen*.3, 1, ilen*.6, 1, ilen*.1, 0

start:	timout	0, idur*i(kjitter), continue
	reinit	start

continue:
anx	gauss	.1
knx	expseg	0.01, .01, .1, .08, .01

anflt1	butbp	anx, inom*1.3, 2000
anoise	=	1+anflt1
anom	=	inom*anoise

anflt2	reson	anx*knx, inom*18, 2600
astrike	=	0;anflt2*.5

k1	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k3	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k4	expseg	0.01, .02, 1.6, .5, .6, 1.5, .8, idur-2, 0.01
k5	expseg	0.01, .02, 1.6, .5, .6, 1.5, .7, idur-2, 0.01
k6	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01

a1	oscil	0.12*k1, anom*cent(ihum), 2
a2	oscil	0.4*k2, anom*cent(iprime), 2
a2d	oscil	0.2*k2, anom*cent(-1163), 2
a3	oscil	0.85*k3, anom*cent(itierce), 2
a4	oscil	0.05*k4, anom*cent(iquint), 2
a5	oscil	1.0*k5, anom, 2
a5a	oscil	0.1*k5, anom*cent(490), 2
a5b	oscil	0.2*k5, anom*cent(510), 2
a6	oscil	0.1*k6, anom*cent(isquint), 2
a7	oscil	0.07*k7, anom*cent(ionom), 2

asig	=	(a1+a2+a2d+a3+a4+a5+a5a+a5b+a6+a7)/10+astrike

aout	=	asig*iamp*3.23*kenv

	outs	aout*idry, aout*idry
gabel	=	gabel+(aout*iwet)
	endin

	instr 97		;stereo dispersion and reverb
			;for modeled bell (instr 5)
idur	=	p3
irvt	=	p4

adel_line	delayr	.01
atap	deltap	.007
	delayw	gabel

adlyL	=	gabel
adlyR	=	atap

arvbL1	comb	adlyL, irvt, .043
arvbL2	comb	adlyL, irvt, .049
arvbL3	=	(arvbL1+arvbL2)*.5
arvbL4	alpass	arvbL3, irvt, .059
arvbL	tone	arvbL4, 4000

arvbR1	comb	adlyR, irvt, .047
arvbR2	comb	adlyR, irvt, .051
arvbR3	=	(arvbR1+arvbR2)*.5
arvbR4	alpass	arvbR3, irvt, .057
arvbR	tone	arvbR4, 4000

	outs	arvbL*1.11, arvbR*.9
gabel	=	0
	endin

	instr 98		;panning control instr for drones
			;(instr 1 & instr 2)
idur	=	p3

kjtr	jitter	.5, .2, .5
kjtr	=	(kjtr+1)*.2

kpan	oscil	.4, kjtr, 3
kpan	=	kpan+.5

gapanL	=	sqrt(kpan)
gapanR	=	sqrt(1-kpan)
	endin

	instr 99		;stereo dispersion and reverb 
			;for pluck (instr 3)
idur	=	p3
irvt	=	p4

adel_line	delayr	0.4
;=====			LEFT
amtap1	deltap	0.0830
amtap2	deltap	0.0620
amtap3	deltap	0.2858
amtap4	deltap	0.3823
amtap5	deltap	0.0203
amtap6	deltap	0.0439
;=====			RIGHT
amtap7	deltap	0.0846
amtap8	deltap	0.2675
amtap9	deltap	0.2067
amtap10	deltap	0.0187
amtap11	deltap	0.1637
amtap12	deltap	0.2276
	delayw	garvb
adiffL	=	(amtap1+amtap2+amtap3+amtap4+amtap5+amtap6)/6
adiffR	=	(amtap7+amtap8+amtap9+amtap10+amtap11+amtap12)/6

arvbL	reverb	adiffL, irvt
arvbR	reverb	adiffR, irvt

aechoL	comb	garvb*.6+arvbR, 3.4, .6
aechoR	comb	garvb*.6+arvbL, 3.8, .6

alopL	tone	arvbL+aechoL, 8000
alopR	tone	arvbR+aechoR, 8000

kenv	linseg	1, idur-1, 1, 1, 0 ;envlp to remove the pop caused by instr shutting off

aoutL	=	alopL*kenv
aoutR	=	alopR*kenv

	outs	aoutL*.54, aoutR*.667
garvb	=	0
	endin

</CsInstruments>
<CsScore>
;saw
f1 0 16384 10 1 .5 .333333 .25 .2 .166667 .142857 .125 .111111 .1 .090909 .083333 .076923
;sine
f2 0 16384 10 1
;triangle
f3 0 16384 10 1 0  .111111 0  .04 0 .020408 0 .012345 0 .008264 0 .005917 ; Band-limited triangle wave

i99	0	54	.8
i98	0	54
i97	0	54	2.8

i5	6	44	10000	-2358	-1159	-881	-440	698	1253	605.6	1.0

i3	0.1	10	4000	8.22
i3	0.2	.	.	9.09
i3	0.3	.	.	9.40
i3	0.4	.	.	9.49

i3	5.0	.	2200	9.40
i3	5.1	.	.	9.09
i3	5.3	.	2800	9.49

i3	10.0	.	2200	9.09
i3	10.5	.	.	10.13
i3	10.6	.	2800	9.49

i3	20.75	.	3200	10.13
i3	20.8	.	.	10.18
i3	21.4	.	3700	8.22
i3	21.4	.	.	8.31

i3	33.2	.	2200	10.04
i3	33.4	.	.	10.09
i3	33.56	.	2800	9.26
i3	33.6	.	.	9.49

i3	38.3	.	.	9.26
i3	38.8	.	3200	9.13
i3	38.9	.	.	9.31

i3	45.7	8	.	8.40
i3	46	.	4000	8.49
i3	46.05	.	.	9.09
i3	46.1	.	.	9.26
i3	46.15	.	.	10.04

i2	3	50	75	11.31
i2	.	.	25	11.29
i2	.	.	75	11.26
i2	.	.	105	11.22
i2	.	.	.	11.18
i2	.	.	30	11.13
i2	.	.	105	11.09

i2	.	.	150	10.00
i2	.	.	.	9.49
i2	.	.	.	9.44
i2	.	.	.	9.40
i2	.	.	.	9.35
i2	.	.	.	9.31

i2	.	.	1200	8.49
i2	.	.	2600	7.26
i2	.	.	2300	7.13

/*
i2	3	51	60	11.31
i2	3	.	100	11.26
i2	3	.	110	11.22
i2	3	.	120	11.18
i2	3	.	150	11.09
i2	3	.	180	11.04
i2	3.1	.	500	8.49
i2	3.1	.	1000	7.18
*/

i1	1.5	34.1	6800	6.40	5	18
i1	5.5	.	10500	7.22	.	.
i1	3.7	.	10000	7.40	4	9
i1	3.62	.	9000	8.00	.	.

i1	11	41.1	6800	6.31	6	6
i1	12.1	40.0	10500	7.18	.	.
i1	13.2	39.9	10000	7.49	8	.
i1	14.3	38.8	9000	8.09	.	.
e
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>938</x>
 <y>72</y>
 <width>198</width>
 <height>1011</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>0</r>
  <g>0</g>
  <b>0</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
<MacGUI>
ioView nobackground {0, 0, 0}
</MacGUI>
