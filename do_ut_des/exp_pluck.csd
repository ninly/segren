<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
;====================
<CsInstruments>
	
sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

gaton	init	0

	instr 1	
idur	=	p3
iamp	=	p4
ipch	=	cpspch(p5)
iatt	=	p6
idec	=	p7

kenv	linseg	0, iatt, 1, idur-(iatt+idec), 1, idec, 0

krnd1	exprand	1
krnd2	rand	1
krnd3	betarand	1, .9, .1

krwb1	rand	.2, 1.1, .1
krwb1	=	krwb1+.2
krwb2	rand	.4, 1.1, .2
krwb2	=	krwb2+.4

kwarb1	oscil	krwb2, krwb1, 2
kwarb	oscil	.5, kwarb1, 3
kvol	=	kwarb+.5

abuzz	gbuzz	kvol, ipch, 16, 12, .81, 2

;apch	=	ipch + aflt5

;aosc1a	oscil	kvol, apch+(krnd1*.01), 1
aosc1a	oscil	kvol, ipch+krnd3, 1
aosc2a	oscil	kvol, ipch*(1+(krnd1*.02)), 1
;aosc2a	oscil	kvol, apch*krnd1, 1
aosc3a	oscil	kvol, ipch*(1-(krnd2*.02)), 1
;aosc3a	oscil	kvol, apch*krnd2, 1
aosca	balance	aosc1a+aosc2a+aosc3a, aosc1a

aflt1	reson	aosca, ipch*5.25, 420
aflt2	reson	aosca, ipch*3.236, 120
aflt3	reson	aosca, ipch*2.1, 20
aflt4	balance	aflt1+aflt2+aflt3, aosca
aflt5	tone	aflt4, ipch*8.4

asig	=	aflt5

aout	=	asig*kenv*iamp
	outs	aout, aout
gaton	=	gaton+asig
	endin

	instr 2
idur	=	p3
iamp	=	p4
ipch	=	cpspch(p5)

kenv	expseg	0.01, .05, 1, 1, .8, idur-6.00, .8, 2, 0.01
kenv	linseg	0, .02, 1, 1, .8, idur-6.00, .8, 2, 0

aosc	oscil	1, ipch, 3

adist	distort1	aosc, 1, 1, 0, 0
asig	=	adist

aout	balance	asig, iamp*(1-gaton)*kenv
	outs	aout, aout
gaton	=	0
	endin

	instr 3
idur	=	p3
iamp	=	p4
ipch	=	cpspch(p5)

kdec	line	1, idur, 0
kenv	lfo	1, 1.2, 5

aplk	pluck	1, ipch, ipch, 0, 1

aflt	tone	aplk, 3*ipch*kenv

asig	=	aflt

aout	=	(asig)*kenv*iamp*kdec

	outs	aout,aout
	endin

</CsInstruments>
;====================
<CsScore>
;saw
f1 0 16384 10 1 .5 .333333 .25 .2 .166667 .142857 .125 .111111 .1 .090909 .083333 .076923
;sine
f2 0 16384 10 1
;triangle
f3 0 16384 10 1 0  .111111 0  .04 0 .020408 0 .012345 0 .008264 0 .005917 ; Band-limited triangle wave

i3	0	10	10000	8.05
i3	0.1	9.9	10000	9.02
i3	0.2	9.8	10000	9.09
i3	0.3	9.7	10000	9.11

i3	4	1	10000	8.11
i3	4.05	.	.	9.02
i3	4.1	.	.	9.06
i3	4.15	.	.	10.01

/*
i2	1	54	100	11.04
i2	1	54	70	11.07
i2	1.1	54	1200	8.11
i2	1.1	54	1200	5.04
i2	4.9	50	4000	6.04

i1	5	31.6	10000	6.09	8	13.2
i1	.	.	.	7.05	.	.
i1	.	.	.	7.09	.	.

i1	23.4	36.6	.	6.07	13.2	6
i1	.	.	.	7.04	.	.
i1	.	.	.	7.11	.	.


;==============
i1	0	30	5000	7.08	4	4
i1	0	30	.	8.08	8	4

i1	5	15	2000	8.11	3	2
i1	18	12	2000	8.10	2	5

i1	25	12	3200	7.10	4	4
i1	.	.	3200	8.01	4	4

;===============
i1	5	31	.	8.11
i1	8	18	.	8.05
i1	12	20	.	7.01
i1	22	10	.	8.04
i1	27	15	.	7.03
i1	28	20	.	7.00
i1	38	22	.	6.05
i1	30	30	.	5.08
*/

</CsScore>
;====================
</CsoundSynthesizer>

