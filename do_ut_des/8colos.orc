sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

gaton	init	0
garvb	init	0

	instr 1	
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53
iatt	=	p6
idec	=	p7

kenv	linseg	0, iatt, 1, idur-(iatt+idec), 1, idec, 0

krnd1	exprand	1
krnd2	rand	1
krnd3	betarand	1, .9, .1

krwb1	rand	.2, 1.1, .1
krwb1	=	krwb1+.2
krwb2	rand	.4, 1.1, .2
krwb2	=	krwb2+.4

kwarb1	oscil	krwb2, krwb1, 2
kwarb	oscil	.5, kwarb1, 3
kvol	=	kwarb+.5

abuzz	gbuzz	kvol, ifrq, 16, 12, .81, 2

;apch	=	ifrq + aflt5

kfrq	=	ifrq+krnd3
;aosc1a	oscil	kvol, ifrq+krnd3, 1
aosc1a	foscil	kvol, 1, kfrq, kfrq*1.75, kvol*1.5, 1
aosc2a	oscil	kvol, ifrq*(1+(krnd1*.02)), 1
aosc3a	oscil	kvol, ifrq*(1-(krnd2*.02)), 1
aosca	balance	aosc1a+aosc2a+aosc3a, aosc1a

aflt1	reson	aosca, ifrq*5.25, 420
aflt2	reson	aosca, ifrq*3.236, 120
aflt3	reson	aosca, ifrq*2.1, 20
aflt4	balance	aflt1+aflt2+aflt3, aosca
aflt5	tone	aflt4, ifrq*8.4

asig	=	aflt5

aout	=	asig*kenv*iamp
	outs	aout, aout
gaton	=	(1-(kvol))
garvb	=	garvb+(aout*.6)
	endin

	instr 2
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53
iatt	=	p6

;kenv	expseg	0.01, .05, 1, 1, .8, idur-6.00, .8, 2, 0.01
kenv	linseg	0, 8, 1, 1, .8, idur-6.00, .8, 2, 0

aosc	oscil	1, ifrq, 3

adist	distort1	aosc, 1, 1, 0, 0
asig	=	adist

aout	balance	asig, iamp*gaton*kenv
	outs	aout, aout
garvb	=	garvb+(aout*.6)
	endin

	instr 3
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53

kdec	line	1, idur, 0
kenv	lfo	1, 1.2, 5

ivib1	cps2pch	10.00, 53
ivib2	cps2pch	10.12, 53
ivib_amt	=	ivib2-ivib1

kvib	oscil	ivib_amt, 3, 2
aplk	pluck	1, ifrq, ifrq, 0, 1

aflt1	tone	aplk, 2*ifrq*kenv
aflt2	reson	aplk, 3000+kvib, 80
aflt3	reson	aplk, 7000+(kvib*.5), 210
aflt4	reson	aplk, 11000+(kvib*.3), 330
aflt	balance	aflt1+aflt2+aflt3+aflt4, aflt1

asig	=	aflt

aout	=	(asig)*kenv*iamp*kdec

	outs	aout,aout

garvb	=	garvb+(aout*.6)
	endin

	instr 4
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53
iatt	=	p6

;kenv	expseg	0.01, .05, 1, 1, .8, idur-6.00, .8, 2, 0.01
kenv	linseg	0, iatt, 1, 1, .8, idur-6.00, .8, 2, 0

aosc	oscil	1, ifrq, 1

adist	distort1	aosc, 1, 1, 0, 0
asig	=	adist

aout	balance	asig, iamp*(1-gaton)*kenv
	outs	aout, aout
garvb	=	garvb+(aout*.6)
	endin

	instr 5	
;idur	=	p3
iamp	=	p4
ihum	=	p5
iprime	=	p6
itierce	=	p7
iquint	=	p8
isquint	=	p9
ionom	=	p10
inom	=	p11

idur	=	12

kjitter	gauss	.1
kjitter	=	1+kjitter

start:	timout	0, idur*i(kjitter), continue
	reinit	start

continue:
anx	rand	.1
knx	expseg	0.01, .01, .1, .08, .01

anflt1	butbp	anx, inom*1.3, 2000
anoise	=	1+anflt1
anom	=	inom*anoise

anflt2	reson	anx*knx, inom*18, 2600
astrike	=	0;anflt2*.5

k1	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2d	expseg	0.01, .03, 1, .5, .6, 1.5, 1, idur-2, 0.01
k3	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k4	expseg	0.01, .02, 1.6, .5, .6, 1.5, .8, idur-2, 0.01
k5	expseg	0.01, .02, 1.6, .5, .6, 1.5, .7, idur-2, 0.01
k5d	expseg	0.01, .02, 1, .5, .6, 1.5, .7, idur-2, 0.01
k6	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7d	expseg	0.01, .01, 1, .5, .7, 1.5, .6, idur-2, 0.01

a1	oscil	0.1*k1, anom*cent(ihum), 2
a2	oscil	0.68*k2, anom*cent(iprime), 2
a3	oscil	1.0*k3, anom*cent(itierce), 2
a4	oscil	0.1*k4, anom*cent(iquint), 2
a5	oscil	0.5*k5, anom, 2
a5d	oscil	0.7*k5d, anom+cent(-17), 2
a6	oscil	0.28*k6, anom*cent(isquint), 2
a7	oscil	0.1*k7, anom*cent(ionom), 2
a7d	oscil	0.06*k7d, anom*cent(ionom)*cent(12), 2

asig	=	(a1+a2+a3+a4+a5+a5d+a6+a7+a7d)/9+astrike

aout	=	asig*iamp*2.5

	outs	aout, aout
	endin

	instr 99
idur	=	p3
irvt	=	p4

adel_line	delayr	0.4
;=====			LEFT
amtap1	deltap	0.0830
amtap2	deltap	0.0620
amtap3	deltap	0.2858
amtap4	deltap	0.3823
amtap5	deltap	0.0203
amtap6	deltap	0.0439
;=====			RIGHT
amtap7	deltap	0.0846
amtap8	deltap	0.2035
amtap9	deltap	0.2067
amtap10	deltap	0.0187
amtap11	deltap	0.1637
amtap12	deltap	0.2276
	delayw	garvb
adiffL	=	(amtap1+amtap2+amtap3+amtap4+amtap5+amtap6)/6
adiffR	=	(amtap7+amtap8+amtap9+amtap10+amtap11+amtap12)/6

arvbL	reverb	adiffL, irvt
arvbR	reverb	adiffR, irvt

aechoL	comb	garvb*.5+arvbR, 3.4, .6
aechoR	comb	garvb*.5+arvbL, 3.8, .6

aoutL	tone	arvbL+aechoL*0.8, 7000
aoutR	tone	arvbR+aechoR, 7000

	outs	aoutL, aoutR
garvb	=	0
	endin
