<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
;====================
<CsInstruments>
	
sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

	instr 1	
idur	=	p3
iamp	=	p4
ihum	=	p5
iprime	=	p6
itierce	=	p7
iquint	=	p8
isquint	=	p9
ionom	=	p10
inom	=	p11

anx	rand	.4
knx	expseg	0.01, .01, .05, .08, .01

anflt1	butbp	anx, inom*1.3, 2000
anoise	=	1+anflt1
anom	=	inom*anoise

anflt2	reson	anx, inom*18, 1600
astrike	=	0;anflt2*knx

k1	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k3	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k4	expseg	0.01, .02, 1.6, .5, .6, 1.5, .8, idur-2, 0.01
k5	expseg	0.01, .02, 1.6, .5, .6, 1.5, .7, idur-2, 0.01
k5d	expseg	0.01, .02, 1, .5, .6, 1.5, .7, idur-2, 0.01
k6	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7d	expseg	0.01, .01, 1, .5, .7, 1.5, .6, idur-2, 0.01

a1	oscil	0.1*k1, anom*cent(ihum), 1
a2	oscil	0.68*k2, anom*cent(iprime), 1
a3	oscil	1.0*k3, anom*cent(itierce), 1
a4	oscil	0.1*k4, anom*cent(iquint), 1
a5	oscil	0.5*k5, anom, 1
a5d	oscil	0.7*k5d, anom+cent(-17), 1
a6	oscil	0.28*k6, anom*cent(isquint), 1
a7	oscil	0.1*k7, anom*cent(ionom), 1
a7d	oscil	0.06*k7d, anom*cent(ionom)*cent(12), 1

asig	=	(a1+a2+a3+a4+a5+a5d+a6+a7+a7d)/9+astrike

aout	=	asig*iamp*2.5

	outs	aout, aout
	endin

</CsInstruments>
;====================
<CsScore>
f1 0 16384 10 1

i1	0	10	10000	-2434	-1188	-904	-547	651	1203	333.2
</CsScore>
;====================
</CsoundSynthesizer>

