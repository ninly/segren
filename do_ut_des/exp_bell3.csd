<CsoundSynthesizer>
;<CsOptions>
;</CsOptions>
;====================
<CsInstruments>
	
sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

	instr 1	
idur	=	p3
iamp	=	p4
ihum	=	p5
iprime	=	p6
itierce	=	p7
iquint	=	p8
isquint	=	p9
ionom	=	p10
inom	=	p11

anx	rand	.2
knx	expseg	0.01, .01, .05, .08, .01

anflt1	butbp	anx, inom*2.3, 2000
anoise	=	1+anflt1
anom	=	inom*anoise

anflt2	reson	anx, inom*18, 1600
astrike	=	0;anflt2*knx

k1	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k3	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k4	expseg	0.01, .02, 1.6, .5, .6, 1.5, .8, idur-2, 0.01
k5	expseg	0.01, .02, 1.6, .5, .6, 1.5, .7, idur-2, 0.01
k6	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01

a1	oscil	0.12*k1, anom*cent(ihum), 2
a2	oscil	0.4*k2, anom*cent(iprime), 2
a2d	oscil	0.2*k2, anom*cent(-1163), 2
a3	oscil	0.85*k3, anom*cent(itierce), 2
a4	oscil	0.05*k4, anom*cent(iquint), 2
a5	oscil	1.0*k5, anom, 2
a5a	oscil	0.1*k5, anom*cent(490), 2
a5b	oscil	0.2*k5, anom*cent(510), 2
a6	oscil	0.1*k6, anom*cent(isquint), 2
a7	oscil	0.07*k7, anom*cent(ionom), 2

asig	=	(a1+a2+a2d+a3+a4+a5+a5a+a5b+a6+a7)/10+astrike

aout	=	asig*iamp*3.23

	outs	aout, aout
	endin

</CsInstruments>
;====================
<CsScore>
f2 0 16384 10 1

i1	0	10	10000	-2358	-1159	-881	-440	698	1253	605.6
</CsScore>
;====================
</CsoundSynthesizer>

