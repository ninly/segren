sr	=	44100
kr	=	4410
ksmps	=	10
nchnls	=	2

gaton	init	0
garvb	init	0
gapan	init	.5

	instr 1	
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53
iatt	=	p6
idec	=	p7

kenv	expseg	0.01, iatt, 1, idur-(iatt+idec), 1, idec, 0.01

krnd1	gauss	1
krnd2	gauss	1
krnd3	betarand	1, .9, .1

krwb1	rand	.2, .1, .1
krwb1	=	krwb1+.2
krwb2	rand	.4, .2, .2
krwb2	=	krwb2+.4

kwarb1	oscil	krwb2, krwb1, 2
kwarb	oscil	.5, kwarb1, 3
kvol	=	kwarb+.5

kfrq	=	ifrq+krnd3
aosc1a	foscil	1, 1, kfrq, kfrq*1.75, kvol*1.5, 1
aosc2a	oscil	1, ifrq*(1+(krnd1*.01)), 1
aosc3a	oscil	1, ifrq*(1-(krnd2*.01)), 1
aosca	balance	aosc1a+aosc2a+aosc3a, aosc1a

aflt1	reson	aosca, ifrq*5.25, 420
aflt2	reson	aosca, ifrq*3.236, 120
aflt3	reson	aosca, ifrq*2.1, 20
aflt4	balance	aflt1+aflt2+aflt3, aosca
aflt5	tone	aflt4, ifrq*8.4

asig	=	aflt5*kvol

aout	=	asig*kenv*iamp
aoutL	=	aout*gapan
aoutR	=	aout*(1-gapan)
	outs	aoutL, aoutR

gaton	=	(1-(kvol))
	endin

	instr 2
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53

kenv	expseg	0.01, 4, 1, idur-8, 1, 4, 0.01

agauss	gauss	5
asmoo	tone	agauss, 20
awarb	oscil	100, asmoo, 2
aosc	oscil	1, ifrq+awarb, 3

asig	=	aosc

aout	balance	asig, iamp*gaton*kenv
aoutL	=	aout*(1-gapan)
aoutR	=	aout*gapan
	outs	aoutL, aoutR

garvb	=	garvb+(aout*.6)
	endin

	instr 3
idur	=	p3
iamp	=	p4
ifrq	cps2pch	p5, 53

kdec	line	1, idur, 0

ivib1	cps2pch	10.00, 53
ivib2	cps2pch	10.12, 53
ivib_amt	=	ivib2-ivib1
kvib	oscil	ivib_amt, 3, 2

aplk	pluck	1, ifrq, ifrq, 0, 1

aflt1	tone	aplk, ifrq
aflt2	reson	aplk, 3000+kvib, 80
aflt3	reson	aplk, 7000+(kvib*.5), 210
aflt4	reson	aplk, 11000+(kvib*.3), 330
aflt5	balance	aflt1+aflt2+aflt3+aflt4, aplk
aflt	tone	aflt5, ifrq*4

asig	=	aflt

aout	=	(asig)*iamp*kdec

	outs	aout,aout*.8

garvb	=	garvb+(aout*.6)
	endin

	instr 5	
ilen	=	p3
iamp	=	p4
ihum	=	p5
iprime	=	p6
itierce	=	p7
iquint	=	p8
isquint	=	p9
ionom	=	p10
inom	=	p11

idur	=	9.5

kjitter	gauss	.2
kjitter	=	1+kjitter

kenv	linseg	.6, ilen*.3, 1, ilen*.6, 1, ilen*.1, 0

start:	timout	0, idur*i(kjitter), continue
	reinit	start

continue:
anx	gauss	.1
knx	expseg	0.01, .01, .1, .08, .01

anflt1	butbp	anx, inom*1.3, 2000
anoise	=	1+anflt1
anom	=	inom*anoise

anflt2	reson	anx*knx, inom*18, 2600
astrike	=	0;anflt2*.5

k1	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k2	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k3	expseg	0.01, .03, 1.5, .5, .6, 1.5, 1, idur-2, 0.01
k4	expseg	0.01, .02, 1.6, .5, .6, 1.5, .8, idur-2, 0.01
k5	expseg	0.01, .02, 1.6, .5, .6, 1.5, .7, idur-2, 0.01
k6	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01
k7	expseg	0.01, .01, 1.7, .5, .6, 1.5, .7, idur-2, 0.01

a1	oscil	0.12*k1, anom*cent(ihum), 2
a2	oscil	0.4*k2, anom*cent(iprime), 2
a2d	oscil	0.2*k2, anom*cent(-1163), 2
a3	oscil	0.85*k3, anom*cent(itierce), 2
a4	oscil	0.05*k4, anom*cent(iquint), 2
a5	oscil	1.0*k5, anom, 2
a5a	oscil	0.1*k5, anom*cent(490), 2
a5b	oscil	0.2*k5, anom*cent(510), 2
a6	oscil	0.1*k6, anom*cent(isquint), 2
a7	oscil	0.07*k7, anom*cent(ionom), 2

asig	=	(a1+a2+a2d+a3+a4+a5+a5a+a5b+a6+a7)/10+astrike

aout	=	asig*iamp*3.23*kenv

	outs	aout, aout
	endin

	instr 98
idur	=	p3

kjtr	jitter	.5, .2, .4
kjtr	=	(kjtr+1)*.3

kpan	oscil	.3, kjtr, 3
kpan	=	kpan+.5

gapan	=	(kpan)
	endin

	instr 99
idur	=	p3
irvt	=	p4

adel_line	delayr	0.4
;=====			LEFT
amtap1	deltap	0.0830
amtap2	deltap	0.0620
amtap3	deltap	0.2858
amtap4	deltap	0.3823
amtap5	deltap	0.0203
amtap6	deltap	0.0439
;=====			RIGHT
amtap7	deltap	0.0846
amtap8	deltap	0.2675
amtap9	deltap	0.2067
amtap10	deltap	0.0187
amtap11	deltap	0.1637
amtap12	deltap	0.2276
	delayw	garvb
adiffL	=	(amtap1+amtap2+amtap3+amtap4+amtap5+amtap6)/6
adiffR	=	(amtap7+amtap8+amtap9+amtap10+amtap11+amtap12)/6

arvbL	reverb	adiffL, irvt
arvbR	reverb	adiffR, irvt

aechoL	comb	garvb*.5+arvbR, 3.4, .6
aechoR	comb	garvb*.5+arvbL, 3.8, .6

alopL	tone	arvbL+aechoL, 7000
alopR	tone	arvbR+aechoR, 7000

aoutL	=	alopL;balance	alopL, garvb
aoutR	=	alopR;balance	alopR, garvb

	outs	aoutL*.6, aoutR*.6
garvb	=	0
	endin
